DROP TABLE IF EXISTS bigdata.PlaysIn, bigdata.actor, bigdata.show, bigdata.producedIn, bigdata.country, bigdata.hasgenre, bigdata.genre CASCADE;

-------------------------------SHOW----------------------------------------

CREATE TABLE bigdata.show AS
  SELECT title
  FROM staging.movies
  GROUP BY title;

ALTER TABLE bigdata.show
  ADD CONSTRAINT pk_Show PRIMARY KEY (title);

CREATE UNIQUE INDEX show_index
  ON bigdata.Show USING BTREE (title);

-- ---------------------------------FILL SHOW--------------------------------------------

ALTER TABLE bigdata.show
  ADD budget BIGINT,
  ADD revenue BIGINT,
  ADD rating FLOAT,
  ADD votes INTEGER,
  ADD runningtime FLOAT,
  ADD type TEXT,
  ADD year TEXT,
  ADD seperateTitle TEXT;

UPDATE bigdata.show
SET revenue = staging.business.revenue,
  budget    = staging.business.budget
FROM staging.business
WHERE bigdata.show.title = staging.business.title;

UPDATE bigdata.show
SET rating = staging.ratings.rating,
  votes    = staging.ratings.votes
FROM staging.ratings
WHERE bigdata.show.title = staging.ratings.title;

UPDATE bigdata.show
SET runningTime = staging.runningtimes.time
FROM staging.runningtimes
WHERE bigdata.show.title = staging.runningtimes.title;

UPDATE bigdata.show
SET type        = staging.movies.type,
  year          = staging.movies.year,
  seperateTitle = staging.movies.seperatetitle
FROM staging.movies
WHERE bigdata.show.title = staging.movies.title;

-- -------------------------------ACTOR----------------------------------------

CREATE TABLE bigdata.actor AS
  SELECT name
  FROM staging.actors
  GROUP BY name;

ALTER TABLE bigdata.actor
  ADD CONSTRAINT pk_Actor PRIMARY KEY (name);

CREATE UNIQUE INDEX actor_index
  ON bigdata.actor USING BTREE (name);

ALTER TABLE bigdata.actor
  ADD birthplace TEXT,
  ADD birthdate TEXT;

UPDATE bigdata.actor
SET birthplace = staging.biographies.birthplace,
  birthdate    = staging.biographies.birthdate
FROM staging.biographies
WHERE bigdata.actor.name = staging.biographies.name;

-- -------------------------------PLAYSIN----------------------------------------

CREATE TABLE bigdata.playsin AS
  SELECT
    staging.movies.title AS showTitle,
    staging.actors.name  AS actorName
  FROM staging.actors
    INNER JOIN staging.movies ON staging.movies.title = staging.actors.title
  GROUP BY (showTitle, actorName);

ALTER TABLE bigdata.playsin
  ADD CONSTRAINT pk_PlaysIn PRIMARY KEY (showTitle, actorName);

CREATE UNIQUE INDEX playIn_index
  ON bigdata.PlaysIn USING BTREE (showTitle, actorName);

ALTER TABLE bigdata.playsin
  ADD CONSTRAINT fk_playsShowTitle
FOREIGN KEY (showTitle)
REFERENCES bigdata.show (title)
ON DELETE CASCADE;

ALTER TABLE bigdata.playsin
  ADD CONSTRAINT fk_playsActorName
FOREIGN KEY (actorname)
REFERENCES bigdata.actor (name)
ON DELETE CASCADE;

-- -------------------------------COUNTRY----------------------------------------

CREATE TABLE bigdata.country AS
  SELECT staging.countries.country AS name
  FROM staging.countries
  GROUP BY name;

ALTER TABLE bigdata.country
  ADD CONSTRAINT pk_Country PRIMARY KEY (name);

CREATE UNIQUE INDEX country_index
  ON bigdata.country USING BTREE (name);

-- -- -------------------------------PRODUCED IN----------------------------------------

CREATE TABLE bigdata.producedin AS
  SELECT
    bigdata.show.title        AS showTitle,
    staging.countries.country AS countryName
  FROM staging.countries
    INNER JOIN bigdata.show ON bigdata.show.title = staging.countries.title
  GROUP BY (showTitle, countryName);

ALTER TABLE bigdata.producedIn
  ADD CONSTRAINT pk_ProductIn PRIMARY KEY (showTitle, countryName);

CREATE UNIQUE INDEX producedIn_index
  ON bigdata.producedIn USING BTREE (showTitle, countryName);

ALTER TABLE bigdata.producedin
  ADD CONSTRAINT fk_countryCountryName
FOREIGN KEY (countryName)
REFERENCES bigdata.country (name)
ON DELETE CASCADE;

ALTER TABLE bigdata.producedin
  ADD CONSTRAINT fk_countryShowTitle
FOREIGN KEY (showTitle)
REFERENCES bigdata.show (title)
ON DELETE CASCADE;

-- -- -- -------------------------------GENRE----------------------------------------

CREATE TABLE bigdata.genre AS
  SELECT staging.genres.genre AS name
  FROM staging.genres
  GROUP BY name;

ALTER TABLE bigdata.genre
  ADD CONSTRAINT pk_Genre PRIMARY KEY (name);

CREATE UNIQUE INDEX genre_index
  ON bigdata.genre USING BTREE (name);

-- -- -- -------------------------------HasGenre----------------------------------------

CREATE TABLE bigdata.hasgenre AS
  SELECT
    bigdata.show.title   AS showTitle,
    staging.genres.genre AS genreName
  FROM staging.genres
    INNER JOIN bigdata.show ON bigdata.show.title = staging.genres.title
  GROUP BY (showTitle, genreName);

ALTER TABLE bigdata.hasgenre
  ADD CONSTRAINT pk_hasGenre PRIMARY KEY (showTitle, genreName);

CREATE UNIQUE INDEX hasGenre_index
  ON bigdata.hasgenre USING BTREE (showTitle, genreName);

ALTER TABLE bigdata.hasgenre
  ADD CONSTRAINT fk_genreName
FOREIGN KEY (genreName)
REFERENCES bigdata.genre (name)
ON DELETE CASCADE;

ALTER TABLE bigdata.hasgenre
  ADD CONSTRAINT fk_genreShowTitle
FOREIGN KEY (showTitle)
REFERENCES bigdata.show (title)
ON DELETE CASCADE;