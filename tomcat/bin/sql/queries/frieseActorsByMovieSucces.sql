SELECT
  bigdata.playsin.actorname,
  sum(show.revenue) / count(show.title) AS avgRevenue
FROM bigdata.playsin
  LEFT JOIN bigdata.show ON bigdata.playsin.showtitle = bigdata.show.title
  INNER JOIN actor ON playsin.actorname = actor.name
WHERE revenue > 0 AND budget > 0 AND lower(actor.birthplace) LIKE '%friesland%'
GROUP BY bigdata.playsin.actorname
ORDER BY avgRevenue DESC
