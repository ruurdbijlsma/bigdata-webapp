SELECT
  showtitle,
  actorname
FROM bigdata.playsin
  LEFT JOIN bigdata.show ON bigdata.playsin.showtitle = bigdata.show.title
WHERE bigdata.playsin.actorname = 'Braakhekke, Joop' AND bigdata.show.type = 'MOVIE'
ORDER BY year ASC