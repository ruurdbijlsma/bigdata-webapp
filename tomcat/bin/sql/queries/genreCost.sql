SELECT
  genrename,
  SUM(budget),
  COUNT(*),
  (SUM(budget) / COUNT(*)) AS avgCost
  (SUM(runningtime) / )
FROM bigdata.hasgenre
  LEFT JOIN bigdata.show ON bigdata.hasgenre.showtitle = bigdata.show.title
WHERE budget IS NOT NULL AND budget > 0 AND budget < 1000000000
GROUP BY genrename
ORDER BY avgcost DESC