SELECT
  country.name AS countryname,
  count(country.name)
FROM country, staging.biographies
WHERE lower(staging.biographies.birthplace) LIKE '%' || lower(bigdata.country.name) || '%'
GROUP BY country.name