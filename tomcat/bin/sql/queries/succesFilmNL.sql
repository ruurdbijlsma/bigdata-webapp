SELECT
  bigdata.show.title,
  bigdata.show.budget,
  bigdata.show.revenue,
  bigdata.show.rating,
  bigdata.show.votes,
  bigdata.show.runningtime,
  bigdata.show.year
FROM bigdata.producedin
  LEFT JOIN bigdata.show on bigdata.producedin.showTitle = bigdata.show.title
WHERE bigdata.producedin.countryName
LIKE 'Netherlands%' AND bigdata.show.type = 'MOVIE' AND revenue IS NOT NULL
ORDER BY revenue DESC
LIMIT 1