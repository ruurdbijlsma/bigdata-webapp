SELECT
  genrename,
  SUM(rating),
  COUNT(*),
  (SUM(rating)/COUNT(*)) as avgRating
FROM bigdata.hasgenre
  LEFT JOIN bigdata.show on bigdata.hasgenre.showtitle = bigdata.show.title
WHERE rating IS NOT NULL
GROUP BY genrename
HAVING COUNT(*) > 1
ORDER BY avgrating DESC