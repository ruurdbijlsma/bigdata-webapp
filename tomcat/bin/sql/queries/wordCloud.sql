SELECT string_agg(t2.naam, ';')
FROM (
       SELECT split_part(t.name, ', ', 2) || ' ' || split_part(t.name, ', ', 1) AS naam
       FROM (
              SELECT name
              FROM bigdata.actor
                LEFT JOIN bigdata.playsin ON name = actorname
                LEFT JOIN bigdata.show ON showtitle = title
              WHERE lower(birthplace) LIKE '%netherlands%' AND type = 'MOVIE'
            ) t) t2