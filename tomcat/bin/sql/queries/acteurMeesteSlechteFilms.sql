SELECT bigdata.playsin.actorname, SUM(bigdata.show.rating), COUNT(*), (SUM(bigdata.show.rating)/COUNT(*)) AS avgRating
FROM bigdata.playsin
LEFT JOIN bigdata.show ON bigdata.playsin.showtitle = bigdata.show.title
WHERE rating IS NOT NULL and rating < 5.5
GROUP BY bigdata.playsin.actorname
ORDER BY count DESC
LIMIT 1