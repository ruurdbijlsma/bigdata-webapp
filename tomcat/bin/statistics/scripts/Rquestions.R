kosten <- function()
{
	library('RPostgreSQL')
	pg = dbDriver("PostgreSQL")
	con = dbConnect(pg, user="bigdata", password="bigdata", host="localhost",port=5432,dbname="IMDB")
	lmdata = dbGetQuery(con, "SELECT runningtime, budget FROM bigdata.show WHERE budget >0 AND runningtime > 0 AND type = 'MOVIE'")
 	on.exit(dbDisconnect(con))
	model=lm(runningtime~budget,data=lmdata)
    values <- list("summary"=paste(capture.output(print(summary(model))), collapse='\n'));

	return(values)
}

SuccesMovie <- function()
{
	library('RPostgreSQL')
	pg = dbDriver("PostgreSQL")
	con = dbConnect(pg, user="bigdata", password="bigdata", host="localhost",port=5432,dbname="IMDB")
	actorsdata = dbGetQuery(con, "SELECT actorname, COUNT(actorname) FROM bigdata.playsin  GROUP BY actorname HAVING COUNT(actorname) > 5")

 	library(caTools)
	set.seed(1234)
	split=sample.split(actorsdata$count,SplitRatio=0.014)
	evenactorsdata=subset(actorsdata, split==TRUE)

	distances = dist(evenactorsdata,method="euclidean")
	clusteractors = hclust(distances, method="ward.D")
	clustergroups = cutree(clusteractors, k=5)

	distances <- NULL
	clusteractors <- NULL

	actorname <- evenactorsdata$actorname

	clustergroep1 = (clustergroups == 1)
	clustergroep2 = (clustergroups == 2)
	clustergroep3 = (clustergroups == 3)
	clustergroep4 = (clustergroups == 4)
	clustergroep5 = (clustergroups == 5)

	clustergroups <- NULL

	count = evenactorsdata$count
	lmactorsdata = data.frame(actorname,count, clustergroep1, clustergroep2,clustergroep3,clustergroep4,clustergroep5)

	clustergroep1 <- NULL
	clustergroep2 <- NULL
	clustergroep3 <- NULL
	clustergroep4 <- NULL
	clustergroep5 <- NULL

	for (i in 3:7)
	{
		newname = changeColNames(lmactorsdata[i], lmactorsdata)
		colnames(lmactorsdata)[i] <- newname
	}

	lmactorsdata[2] <- NULL

	newdata2 = dbGetQuery(con, "SELECT * FROM bigdata.show INNER JOIN bigdata.playsin ON bigdata.show.title = bigdata.playsin.showtitle WHERE bigdata.show.type LIKE 'MOVIE' AND bigdata.show.rating >= 0 AND bigdata.show.budget >= 0 AND bigdata.show.revenue >=0")
	showactorcomplete = merge(x = newdata2 , y = lmactorsdata, by = "actorname")
	newdata2 <- NULL
	lmactorsdata <- NULL
	showactorcomplete$Succes <- as.factor(showactorcomplete$rating > 6.5 & showactorcomplete$votes > 1000 & showactorcomplete$revenue > showactorcomplete$budget)

	on.exit(dbDisconnect(con))

	split=sample.split(showactorcomplete$Succes,SplitRatio=0.65)
	train=subset(showactorcomplete, split==TRUE)
	test=subset(showactorcomplete, split==FALSE)

 	variables = c(colnames(train)[11],colnames(train)[12],colnames(train)[13],colnames(train)[14],colnames(train)[15],colnames(train)[6])
	fmla <- as.formula(paste("Succes ~ ", paste(variables , collapse= "+")))
	model=glm(fmla,data=train,family=binomial)

	while(TRUE)
	{
		fmla <- as.formula(paste("Succes ~ ", paste(variables , collapse= "+")))
		model=glm(fmla,data=train,family=binomial)
		coefficient = 0.1
		variable = ""
		for (i in 2: length(all.vars(formula(model)[-2])))
		{
			if (coef(summary(model))[,"Pr(>|z|)"][i] >= coefficient)
			{
				coefficient = coef(summary(model))[,"Pr(>|z|)"][i]
				variable = all.vars(formula(model)[-2])[i-1]
			}
		}
		if (!(variable == ""))
		{
			variables = setdiff(variables, variable)
		}
		else
		{
			break
		}
	}

	predictTest=predict(model,type="response",newdata=test)
	library(ROCR)
	ROCRpred=prediction(predictTest,test$Succes)

	ROCRperf=performance(ROCRpred,"tpr","fpr")
	jpeg('statistics/results/rplot.jpg')
	plot(ROCRperf,colorize=TRUE,print.cutoffs.at=seq(0,1,0.1))
	abline(0,1)
	dev.off()

	AUC=as.numeric(performance(ROCRpred,"auc")@y.values)

    values <- list("summary"=paste(capture.output(print(summary(model))), collapse='\n'), "AUC"=AUC);
    listAllValues = ConfusionMatrix(predictTest,test$Succes,values)
    return(listAllValues)
}

ConfusionMatrix <- function(predictionr ,afhvar, sumAUCvalues)
{
	confusionMatrix <- table(unname((afhvar),force = TRUE), predictionr > 0.5)

	accuracy = sum(diag(confusionMatrix))/sum(confusionMatrix)
	specificity = confusionMatrix[1,1] / (confusionMatrix[1,1] + confusionMatrix[1,2])
	sensitivity = confusionMatrix[2,2] / (confusionMatrix[2,2] + confusionMatrix[2,1])

    confusionValues <- list("confusionMatrix"= paste(capture.output(print(confusionMatrix)), collapse='\n'),"acc" = accuracy,"sens" = sensitivity,"spec" =specificity);

	allValues <- append(sumAUCvalues,confusionValues)
	return(allValues)
}

changeColNames <- function(colname,data)
{
	L = colname == TRUE
	op = data[L,]
	mi = min(op$count)
	ma = max(op$count)
	newp = capture.output(cat('clustergroep',mi,ma,sep="."))
	return (newp)
}

