<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:layout title="SQL Results - Acteur meest slechte films" section="menu_sqlresults">

  <jsp:attribute name="body_area">
    <script src="js/actorfilm.js"></script>
    <h1 class="page-header">SQL Results - Acteur in Films</h1>
        <div class="col-lg-8 col-lg-offset-2">
            <div class="col-lg-12 text-answer"><p id="actorName"></p></div>
            <div class="question"><p>In welke films speelde Joop Braakhekke?</p></div>
        </div>

</jsp:attribute>

</t:layout>