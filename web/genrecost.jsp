<%--suppress CssUnusedSymbol --%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:layout title="Gemiddelde kosten per genre" section="menu_visualisations">

  <jsp:attribute name="body_area">
    <h1 class="page-header">Visualisaties - Gemiddelde kosten per genre</h1>
      <style>

          .bar {
              fill: firebrick;
          }

          .bar:hover {
              fill: dimgrey;
          }

          .axis--x path {
              display: none;
          }

          span{
              font-size: 20px;
          }

          .genre{
              font-size: 22px;
              font-weight: bold;
          }

          .cost{
              font-size: 22px;
              font-weight: bold;
          }

      </style>
          <svg width="1080" height="500"></svg>
          <script src="https://d3js.org/d3.v4.min.js"></script>
          <script src="js/genrecost.js"></script>
      <div class="col-lg-12">
          <span>Kosten voor </span><span class="genre">... </span><span>: &euro;</span><span class="cost"></span>
      </div>
</jsp:attribute>

</t:layout>