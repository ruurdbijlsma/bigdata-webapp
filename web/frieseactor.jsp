<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:layout title="SQL Results - Friese acteurs meest succesvolle films" section="menu_sqlresults">

  <jsp:attribute name="body_area">
    <script src="js/frieseactor.js"></script>
    <div class='background-image'>
    </div>
    <h1 class="page-header">SQL Results - Friese acteurs, meest succesvolle films</h1>
        <div class="col-lg-8 col-lg-offset-2">
            <div class="col-lg-6 col-lg-offset-3 text-answer"><p id="frieseActorName"></p></div>
            <div class="question"><p>Welke Friese acteurs spelen in de meest succesvolle films?</p></div>
        </div>
</jsp:attribute>

</t:layout>