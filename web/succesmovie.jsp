<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:layout title="R vragen - verband tussen kosten en film" section="menu_statistics">

  <jsp:attribute name="body_area">
    <script src="js/succesmoviescript.js"></script>
    <h1 class="page-header">R vragen - Succesvolle Film</h1>
        <div class="col-lg-8 col-lg-offset-2">
            <div class="col-lg-12 text-answer">
                <h2>Samenvatting model:</h2>
                <pre id="summaryVerband"></pre>
                <h2>ROC Curve:</h2>
                <img src='statistics?name=ROC'></img>
                <h2>AUC:</h2>
                <pre id="AUC"></pre>
                <h2>Confusion Matrix:</h2>
                <pre id="confusion"></pre>
                <h2>Uitleg</h2>
                <p>
                    Om de vraag duidelijk te beantwoorden werden aan de afhankelijke variabele de volgende eisen
                    gesteld:
                <ul>
                <li>De film moest een rating van 6.5 of hoger hebben.</li>
                <li>De hoeveelheid stemmen (votes) moesten hoger zijn dan 1000.</li>
                <li>De opbrengst moet hoger zijn dan het budget.</li>
                </ul>
                Als een factor die kan leiden tot een succesvolle film werden de acteurs opgedeeld in vijf clusters op
                basis van de hoeveelheid films de acteur in heeft gespeeld. Voor het verbeteren van de resultaten werden
                de acteurs die in minder dan zes films gespeeld hadden buiten beschouwing gelaten.
                <br>
                Er werden er zes factoren uitgeprobeerd die zouden kunnen leiden tot een succesvolle film, waarvan er
                vijf de clusters van de acteurs waren en de zesde de hoeveelheid stemmen (votes).
                <br>
                Uiteindelijk valt uit de samenvatting op te maken dat er 4 factoren van belang zijn, waaronder het
                aantal votes. Met het verkregen model kan een redelijk goede voorspelling worden gemaakt van het aantal
                films dat succesvol is. Dit is te onderbouwen met de verkregen ROC Curve en de bijbehorende AUC waarde
                van 0.78.
                Uit de confusion matrix wordt duidelijk dat er veel true negatives worden gevonden, terwijl het aantal
                true positives beduidend lager is. Het aantal false negatives is bijna vier keer groter dan het aantal
                true positives, wat zorgt voor een lage sensitivity van 0.24. Doordat het aantal false positives vrij
                laag ligt, zeker ten opzichte van het aantal true negatives, levert dat een specificity op van 0.98. De
                accuracy van dit model op basis van de confusion matrix is 0.85, wat duidt op een redelijk goed model.

                </p>
            </div>
            <div class="question"><p>Welke factoren leiden tot een succesvolle film?</p></div>
        </div>

</jsp:attribute>

</t:layout>
