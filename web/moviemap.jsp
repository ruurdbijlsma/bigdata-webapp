<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:layout title="Visualizations - Movie Map" section="menu_visualisations">

  <jsp:attribute name="body_area">
    <h1 class="page-header">Visualizations - Movie Map</h1>
    <iframe src='movieframe.html'></iframe>
	<div class="col-lg-8 col-lg-offset-2">
      <div class="question"><p>In welk land is de film uitgegeven?</p></div>
    </div>
</jsp:attribute>

</t:layout>