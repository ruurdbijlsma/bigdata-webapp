<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:layout title="R vragen - verband tussen kosten en film" section="menu_statistics">

  <jsp:attribute name="body_area">
    <script src="js/kostenscript.js"></script>
    <h1 class="page-header">R vragen - verband tussen kosten en film</h1>
        <div class="col-lg-8 col-lg-offset-2">
            <div class="col-lg-12 text-answer">
                <h2>Samenvatting model:</h2>
                <pre id="summaryVerband"></pre>
                <h2>Uitleg</h2>
                <p>Aangezien we een uiterst lage Multiple R-squared hebben kunnen we stellen dat er geen verband is tussen de kosten en de lengte van een film.</p>
            </div>
            <div class="question"><p>Is er een significant verband tussen de kosten en de lengte van een film?</p></div>
        </div>

</jsp:attribute>

</t:layout>
