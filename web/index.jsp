<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:layout title="Overview" section="menu_overview">

  <jsp:attribute name="body_area">
      <script src="js/overview.js"></script>
    <h1 class="page-header">Bigdata project Groep 12</h1>
      <div class="col-lg-8 col-lg-offset-2">
          <div class="col-lg-12 text-answer">
              In het menu links op de pagina kunt u navigeren naar de verschillende onderdelen van deze website.
              Op deze pagina hebben wij een aantal leuke feitjes verzameld.
          </div>
          <div class="col-lg-12 text-answer">
              Onze database bevat:
              <ul>
                  <li id="actors"></li>
                  <li id="shows"></li>
                  <li id="dbsize"></li>
              </ul>
          </div>
          <div class="row">
              <div class="col-lg-4"><div class="overviewImg" onclick="location.href='wordcloud.jsp'" style="background-image:url(/img/wordcloud.png)"></div></div>
              <div class="col-lg-4"><div class="overviewImg" onclick="location.href='actormap.jsp'" style="background-image:url(/img/actormap.png)"></div></div>
              <div class="col-lg-4"><div class="overviewImg" onclick="location.href='numbers.jsp'" style="background-image:url(/img/benford.png)"></div></div>
          </div>

          <div class="row">
              <div class="col-lg-4"><div class="overviewImg" onclick="location.href='genrecost.jsp'" style="background-image:url(/img/genrecost.png)"></div></div>
              <div class="col-lg-4"><div class="overviewImg" onclick="location.href='genrerating.jsp'" style="background-image:url(/img/genrerating.png)"></div></div>
              <div class="col-lg-4"><div class="overviewImg" onclick="location.href='moviemap.jsp'" style="background-image:url(/img/moviemap.png)"></div></div>
          </div>


      </div>
</jsp:attribute>

</t:layout>