<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:layout title="SQL Results - Meest opgebrachte film NL" section="menu_sqlresults">

  <jsp:attribute name="body_area">
    <script src="js/meestopgebracktnlscript.js"></script>
    <h1 class="page-header">SQL Results - Meest opgebrachte film NL</h1>
        <div class="col-lg-8 col-lg-offset-2">
            <div class="col-lg-12 text-answer">De meest succesvolle film in Nederland is <strong id="filmTitle"></strong>, met een budget van &euro;<strong id="filmBudget"></strong> euro heeft de film een opbrengst behaald van &euro;<strong id="filmRevenue"></strong> euro. De film behaalde een rating van een <strong id="filmRating"></strong> met een totaal aantal stemmen van <strong id="filmVotes"></strong>.</div>
            <div class="question"><p>Welke Nederlandse film heeft het meeste opgebracht?</p></div>
        </div>

</jsp:attribute>

</t:layout>