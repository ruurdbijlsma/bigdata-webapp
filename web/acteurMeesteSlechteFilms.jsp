<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:layout title="SQL Results - Acteur meeste slechte films" section="menu_sqlresults">

  <jsp:attribute name="body_area">
    <script src="js/acteurMeestSlechteFilmsscript.js"></script>
    <h1 class="page-header">SQL Results - Acteur meeste slechte films</h1>
        <div class="col-lg-8 col-lg-offset-2">
            <div class="col-lg-12 text-answer"><strong id="actorName"></strong> speelt het vaakst in slecht gewaardeerde films. Maar liefst <strong id="filmCount"></strong> keer speelde hij/zij in een slechte film, deze films behaalden gemiddeld een rating van <strong id="filmRating"></strong>.</div>
            <div class="question"><p>Welke acteur of actrice speelt het vaakst in slecht gewaardeerde films?</p></div>
        </div>

</jsp:attribute>

</t:layout>