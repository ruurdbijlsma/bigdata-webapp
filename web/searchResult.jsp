<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:layout title="Visualizations - Movie Map" section="search">

  <jsp:attribute name="body_area">
    <script src="js/resultscript.js"></script>
    <h1 id="movieTitle"></h1>
      <h3>Info:</h3>
    <p id="movieInfo">Laden...</p>
      <h3>Cast:</h3>
    <p id="actors">Laden...</p>
</jsp:attribute>

</t:layout>