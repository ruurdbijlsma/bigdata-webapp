<%--suppress CssUnusedSymbol --%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:layout title="Nummers in filmtitels" section="menu_visualisations">

  <jsp:attribute name="body_area">
	<h1>Visualisaties - Nummers in filmtitels</h1>
	  <style>

		  .bar {
			  fill: firebrick;
		  }

		  .bar:hover {
			  fill: dimgrey;
		  }

		  .axis--x path {
			  display: none;
		  }

	  </style>

	<script src="https://d3js.org/d3.v4.min.js"></script>
	<script src="js/numberscript.js"></script>
	<svg width="1080" height="500"></svg>
	<div class="col-lg-8 col-lg-offset-2">
		<div class="question">
			<p>Hoe vaak komt een bepaald getal in een filmtitel voor?</p>
		</div>
	</div>
</jsp:attribute>

</t:layout>