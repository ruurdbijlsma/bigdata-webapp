<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:layout title="Visualizations - Actor Map" section="menu_visualisations">

  <jsp:attribute name="body_area">
    <h1 class="page-header">Visualizations - Actor Map</h1>
    <iframe src='actorframe.html'></iframe>
	<div class="col-lg-8 col-lg-offset-2">
        <div class="question"><p>In welk land zijn acteurs geboren?</p></div>
    </div>
</jsp:attribute>

</t:layout>