/**
 * Created by willemdejong on 17-01-17.
 */


function updateResults() {
    if (confirm("This action will take some minutes, are you sure?")) {
        $.ajax({
            method: "POST",
            url: "/update",
            data: {action: "results"}
        })
            .done(function (msg) {
                console.debug(msg);
            });
    }

}

function updateStatistics() {
    if (confirm("This action will take some minutes, are you sure?")) {
        $.ajax({
            method: "POST",
            url: "/update",
            data: {action: "statistics"}
        })
            .done(function (msg) {
                console.debug(msg);
            });
    }

}

function updateDB() {
    if (confirm("This action will take an hour, are you sure?")) {
        $.ajax({
            method: "POST",
            url: "/update",
            data: {action: "database"}
        })
            .done(function (msg) {
                console.debug(msg);
            });
    }

}
