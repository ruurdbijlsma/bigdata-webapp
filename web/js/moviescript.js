document.addEventListener('DOMContentLoaded', () => {
    $.ajax({
            method: "get",
            url: "../getResult",
            data: { name: "moviesCountry" }
        })
        .done(function(input) {
            map = new WorldMap(cleanInput(input));
        });
});

function cleanInput(input) {
    input= input.sort((a,b)=>a.count-b.count);

    let values = [];
    for (let row of input) {
        let val = row.count;
        values.push(val);
    }

    let paletteScale = getPallete(values),
        data = {};

    for (let row of input) {
        let val = row.count;
        isoKey = getCountryISO(row.countryname);

        if (!data[isoKey])
            data[isoKey] = {
                numberOfThings: 0,
                fillColor: paletteScale(0)
            };
        data[isoKey].numberOfThings += val;
        data[isoKey].fillColor = paletteScale(val + data[isoKey].numberOfThings);
    }
    return data;
}

function getPallete(values, lowColor = '#DDDDFF', highColor = '#02386f') {
    let minValue = Math.min.apply(null, values),
        maxValue = Math.max.apply(null, values);
    return d3.scale.linear()
        .domain([minValue, maxValue])
        .range([lowColor, highColor]);
}
