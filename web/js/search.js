request = false;

function search(e) {
    if (e.key == "Enter") {
        console.log(e.target.value);
        if (request) request.abort();
        request = $.ajax({
            method: "get",
            url: "../search",
            data: {query: e.target.value}
        })
            .done(function (result) {
                let html = '';
                for (let show of result) {
                    html += `<option value='${show.title}'>${show.rating}</option>`
                }
                $('#suggestions').html(html);
                console.debug(result);
            });
    }
}

function onInput(e) {
    let val = e.target.value;
    let options = document.getElementById('suggestions').childNodes;
    for (let option of options) {
        if (option.value === val) {
            location.href = "/searchResult.jsp?" + option.value;
        }
    }
}