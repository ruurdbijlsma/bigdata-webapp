document.addEventListener('DOMContentLoaded', () => {
    $.ajax({
        method: "get",
        url: "../getResult",
        data: {name: "totalActors"}
    })
        .done(function (input) {
            result = input;
            $('#actors').text(result[0].count.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + " acteurs en actrices");
        })
});

document.addEventListener('DOMContentLoaded', () => {
    $.ajax({
        method: "get",
        url: "../getResult",
        data: {name: "totalShows"}
    })
        .done(function (input) {
            result = input;
            $('#shows').text(result[0].count.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + " films, series en programma's");
        })
});

document.addEventListener('DOMContentLoaded', () => {
    $.ajax({
        method: "get",
        url: "../getResult",
        data: {name: "dbsize"}
    })
        .done(function (input) {
            result = input;
            $('#dbsize').text(result[0].pg_size_pretty + " aan data");
        })
});