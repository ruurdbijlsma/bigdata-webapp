document.addEventListener('DOMContentLoaded', () => {
    console.log('start');
    $.ajax({
            method: "get",
            url: "../statistics",
            data: { name: 'SuccesMovie' }
        })
        .done(function(result) {
            list = result.split('$$$$$$').map(r => r.trim());
            console.log(list);
            summaryVerband.innerHTML = list[0];
            AUC.innerHTML = list[1];
            confusion.innerHTML = `<p>         ${list[2]}</p>`;
            confusion.innerHTML += `<p>Accuracy: ${list[3]}</p>`;
            confusion.innerHTML += `<p>Sensitivity: ${list[4]}</p>`;
            confusion.innerHTML += `<p>Specificity: ${list[5]}</p>`;
        });
});
