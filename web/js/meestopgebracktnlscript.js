/**
 * Created by willemdejong on 20-01-17.
 */
document.addEventListener('DOMContentLoaded', () => {
    $.ajax({
        method: "get",
        url: "../getResult",
        data: { name: "succesFilmNL" }
    })
        .done(function(input) {
            result = input;
            $("#filmTitle")[0].innerHTML = result[0].title;
            $("#filmBudget")[0].innerHTML = result[0].budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            $("#filmRevenue")[0].innerHTML = result[0].revenue.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            $("#filmRating")[0].innerHTML = result[0].rating;
            $("#filmVotes")[0].innerHTML = result[0].votes;
        });


});


