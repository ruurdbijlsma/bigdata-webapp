/**
 * Created by willemdejong on 23-01-17.
 */
document.addEventListener('DOMContentLoaded', () => {
    $.ajax({
            method: "get",
            url: "../getResult",
            data: { name: "frieseActorsByMovieSucces" }
        })
        .done(function(result) {
            console.log(result);
            let html = '<span>Acteur</span><span class=\'rightactor\'>Gem. winst per film</span><ul id="lijst">';
            for (let actor of result) {
                let name = actor.actorname.split(', ');
                name = name[1] + ' ' + name[0];
                html += `<li><span>${name}</span><span class='rightactor'>&euro;${actor.avgrevenue.toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}</span></li>`;
            }
            $("#frieseActorName").html(html + "</ul>");
        });
});