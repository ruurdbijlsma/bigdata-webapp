class WorldMap {
    constructor(data, tooltip = 'Amount of movies in', rotationX = 0, rotationY = 0, mouseInteractivity = true) {
        this.data = data;
        this.tooltip = tooltip;

        this.showLegend(10, 10, 200, 20);

        let container = $('#mapContainer');
        this.width = container.width();
        this.height = container.height();
        window.addEventListener("resize", () => {
            this.width = container.width();
            this.height = container.height();
            this.updateMap(this.projection);
        });

        this.projection = d3.geo.orthographic()
            .scale(200)
            .translate([this.width / 2, this.height / 2])
            .clipAngle(90)
            .precision(.1);

        this.rotation = {
            x: rotationX,
            y: rotationY
        };

        this.mouse = {
            down: false,
            startPos: {
                x: 0,
                y: 0
            }
        };

        if (mouseInteractivity) {
            document.addEventListener('mousedown', e => {
                this.mouse.down = true;
                this.mouse.prevPos = {
                    x: e.pageX,
                    y: e.pageY
                };
            });
            document.addEventListener('mousemove', e => {
                if (this.mouse.down) {
                    let x = e.pageX,
                        y = e.pageY;
                    this.translate = {
                        x: x - this.mouse.prevPos.x,
                        y: (y - this.mouse.prevPos.y) * -1
                    };
                    this.mouse.prevPos = {x: x, y: y};
                    this.rotation = {
                        x: this.rotation.x + this.translate.x * this.rotationSpeed,
                        y: this.rotation.y + this.translate.y * this.rotationSpeed
                    }
                }
            });
            document.addEventListener('mouseup', () => {
                this.mouse.down = false;
            });
            document.addEventListener('wheel', e => {
                if (e.target.toString().includes("SVG")) {
                    e.preventDefault();
                    this.zoom -= e.deltaY * this.zoom / 2000;
                }
            });
        }

        document.addEventListener('keydown', e => {
            if (e.key == 'r')
                if (this.rotateInterval) {
                    this.stopRotating();
                } else {
                    this.startRotating();
                }
        });
    }

    showLegend(x = 0, y = 0, width = 200, height = 20) {
        height += 50;

        let canvas = document.createElement('canvas');
        let context = canvas.getContext('2d');

        document.body.appendChild(canvas);

        canvas.style.position = 'fixed';
        canvas.style.top = x + 'px';
        canvas.style.left = y + 'px';
        canvas.style.width = width + 'px';
        canvas.style.height = height + 'px';
        context.width = width;
        context.height = height;
        canvas.width = width;
        canvas.height = height;


        let lowest = Infinity;
        let highest = -Infinity,
            lowestColor, highestColor;
        for (let country in this.data) {
            let value = this.data[country];
            if (value.numberOfThings < lowest) {
                lowest = value.numberOfThings;
                lowestColor = value.fillColor;
            }
            if (value.numberOfThings > highest) {
                highest = value.numberOfThings;
                highestColor = value.fillColor;
            }
        }
        console.log(lowest, lowestColor, highest, highestColor);

        let grd = context.createLinearGradient(0, 0, width, 0);
        grd.addColorStop(0, lowestColor);
        grd.addColorStop(1, highestColor);

        context.fillStyle = grd;
        context.fillRect(0, 0, width, height - 50);

        context.fillStyle = 'black';
        context.font = '20px arial';
        context.fillText(lowest, 0, height - 50 + 20);

        let textWidth = context.measureText(highest).width;
        context.fillText(highest, width - textWidth, height - 50 + 20);

        console.log(context);
    }

    get zoom() {
        return this.projection.scale();
    }

    set zoom(v) {
        this.updateMap(this.projection.scale(v));
    }

    startRotating(direction = 1, fps = 30) {
        this.rotateInterval = setInterval(() => {
            this.rotation = {
                x: this.rotation.x + direction,
                y: this.rotation.y
            };
        }, 1000 / fps)
    }

    stopRotating() {
        if (this.rotateInterval) {
            clearInterval(this.rotateInterval);
            delete this.rotateInterval;
        }
    }

    get rotationSpeed() {
        return 100 / this.zoom;
    }

    get rotation() {
        return this._rotation;
    }

    set rotation(r) {
        this._rotation = r;
        this.updateMap(this.projection.rotate([r.x, r.y]))
    }

    get map() {
        return this._map();
    }

    updateMap(projection) {
        document.getElementById('mapContainer').innerHTML = "";
        window.removeEventListener('resize', () => this._map.resize());

        let path = d3.geo.path()
            .projection(projection);

        //generate fills
        // let fills = {
        //     defaultFill: '#DDD'
        // };
        // fills[this.lowest] = this.lowestColor;
        // fills[this.highest] = this.highestColor;

        this._map = new Datamap({
            element: document.getElementById('mapContainer'),
            setProjection: () => ({
                path: path,
                projection: projection
            }),
            fills: {
                defaultFill: '#DDD'
            },
            data: this.data,
            borderColor: '#CCC',
            highlightBorderWidth: 2,
            highlightBorderColor: '#B7B7B7',
            highlightFillColor: function (geo) {
                return geo.fillColor || '#F5F5F5';
            },
            geographyConfig: {
                popupTemplate: (geo, data) => {
                    return `<div class="hoverinfo"><strong>
                        ${this.tooltip} ${geo.properties.name}: 
                        ${data.numberOfThings}
                        </strong></div>
                    `;
                }
            },
            responsive: true
        });

        window.addEventListener('resize', () => this._map.resize());
    }
}
