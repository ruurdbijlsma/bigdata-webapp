document.addEventListener('DOMContentLoaded', () => {
    $.ajax({
        method: "get",
        url: "../getResult",
        data: {name: "genreRating"}
    })
        .done(function (input) {
            data = input;
            const svg = d3.select("svg"),
                margin = {top: 20, right: 20, bottom: 80, left: 40},
                width = +svg.attr("width") - margin.left - margin.right,
                height = +svg.attr("height") - margin.top - margin.bottom;

            const x = d3.scaleBand().rangeRound([0, width]).padding(0.1),
                y = d3.scaleLinear().rangeRound([height, 0]);

            const g = svg.append("g")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

            x.domain(data.map(function (d) {
                return d.genrename;
            }));
            y.domain([5, d3.max(data, function (d) {
                return d.avgrating;
            })]);

            g.append("g")
                .attr("class", "axis axis--x")
                .attr("transform", "translate(0," + height + ")")
                .call(d3.axisBottom(x))
                .selectAll("text")
                .attr("y", 0)
                .attr("x", -10)
                .attr("dy", ".35em")
                .attr("transform", "rotate(-70)")
                .style("text-anchor", "end");

            g.append("g")
                .attr("class", "axis axis--y")
                .call(d3.axisLeft(y).ticks(10, ""))
                .append("text")
                .attr("transform", "rotate(-90)")
                .attr("y", 6)
                .attr("dy", "0.71em")
                .attr("text-anchor", "end")
                .text("Rating");

            g.selectAll(".bar")
                .data(data)
                .enter().append("rect")
                .attr("class", "bar")
                .attr("x", function (d) {
                    return x(d.genrename);
                })
                .attr("y", function (d) {
                    return y(d.avgrating);
                })
                .attr("width", x.bandwidth())
                .attr("height", function (d) {
                    return height - y(d.avgrating);
                });

            svg.selectAll("text.bar")
                .data(data)
                .enter().append("text")
                .attr("class", "rtext")
                .attr("text-anchor", "middle")
                .attr("x", function(d) { return x(d.genrename) + 55; })
                .attr("y", function(d) { return y(d.avgrating) + 45; })
                .text(function(d) { return d.avgrating.toFixed(1); });



        });
});