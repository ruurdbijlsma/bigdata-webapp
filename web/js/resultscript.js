document.addEventListener('DOMContentLoaded', () => {
    let query = decodeURI(location.search).substr(1);
    movieTitle.innerHTML = query;
    $.ajax({
        method: "get",
        url: "../search",
        data: {query: query}
    })
        .done(function (data) {
            let movie = data[0];
            let budget, revenue;
            if(movie.budget == null){
                budget = " niet bekend"
            }
            else{
                budget = movie.budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
            }
            if(movie.revenue == null){
                revenue = " niet bekend"
            }
            else{
                revenue = movie.revenue.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
            }
            movieInfo.innerHTML = '';
            movieInfo.innerHTML = `<p>Rating: ${movie.rating}</p>
                      <p>Votes: ${movie.votes.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}</p>
                      <p>Runningtime: ${Math.floor(movie.runningtime / 60) + ':' + movie.runningtime % 60} uur</p> 
                      <p>Budget:  &euro;${budget}</p>
                      <p>Revenue: &euro;${revenue}</p>
                      `;
        });

    $.ajax({
        method: "get",
        url: "../actor",
        data: {movie: query}
    })
        .done(function (data) {
            let html = '';
            for (let actor of data) {
                html += `<p>${actor.actorname}</p>`;
            }
            actors.innerHTML = '';
            actors.innerHTML = html;
        });
});