document.addEventListener('DOMContentLoaded', () => {
    $.ajax({
            method: "get",
            url: "../getResult",
            data: { name: "allNumbers" }
        })
        .done(function(data) {
            createSVG(countem(data[0].string_agg));
        });
});

function createSVG(data) {

    const svg = d3.select("svg"),
        margin = {top: 20, right: 20, bottom: 80, left: 40},
        width = +svg.attr("width") - margin.left - margin.right,
        height = +svg.attr("height") - margin.top - margin.bottom;

    const x = d3.scaleBand().rangeRound([0, width]).padding(0.1),
        y = d3.scaleLinear().rangeRound([height, 0]);

    const g = svg.append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    x.domain(data.map(function(d) {
        return d.number;
    }));
    y.domain([0, 30]);

    g.append("g")
        .attr("class", "axis axis--x")
        .attr("transform", "translate(0," + height + ")")
        .call(d3.axisBottom(x));

    g.append("g")
        .attr("class", "axis axis--y")
        .call(d3.axisLeft(y).ticks(10, "s"))
        .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", "0.71em")
        .attr("text-anchor", "end")
        .text("Rating");

    g.selectAll(".bar")
        .data(data)
        .enter().append("rect")
        .attr("class", "bar")
        .attr("x", function(d) {
            return x(d.number);
        })
        .attr("y", function(d) {
            return y(d.percentage);
        })
        .attr("width", x.bandwidth())
        .attr("height", function(d) {
            return height - y(d.percentage);
        });

    svg.selectAll("text.bar")
        .data(data)
        .enter().append("text")
        .attr("class", "rtext")
        .attr("text-anchor", "middle")
        .attr("x", function(d) { return x(d.number) + 90; })
        .attr("y", function(d) { return y(d.percentage) + 45; })
        .text(function(d) { return d.percentage.toFixed(0) + "%"; });
}


function countem(data) {
    let occurances = [
        { number: 1, percentage: 0 },
        { number: 2, percentage: 0 },
        { number: 3, percentage: 0 },
        { number: 4, percentage: 0 },
        { number: 5, percentage: 0 },
        { number: 6, percentage: 0 },
        { number: 7, percentage: 0 },
        { number: 8, percentage: 0 },
        { number: 9, percentage: 0 }
    ];
    for (let i = 0; i < data.length; i++) {
        let key = data[i] - 1;
        if (data[i] > 0) {
            occurances[key].percentage++;
        }
    }

    for (let occurance of occurances) {
        occurance.percentage /= data.length / 100;
        occurance.percentage = Math.floor(occurance.percentage * 10) / 10;
    }

    return occurances;
}
