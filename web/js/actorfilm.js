/**
 * Created by willemdejong on 23-01-17.
 */
document.addEventListener('DOMContentLoaded', () => {
    $.ajax({
        method: "get",
        url: "../getResult",
        data: {name: "actorfilm"}
    })
        .done(function (result) {
            console.log(result);
            let html = '<p>De films waarin Joop Braakhekke heeft gespeeld:</p><ul>';
            for (let actor of result) {
                html += `<li>${actor.showtitle}</li>`;
            }
            $("#actorName").html(html + "</ul>");

            //#films
        });
});


