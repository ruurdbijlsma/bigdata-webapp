/**
 * Created by willemdejong on 20-01-17.
 */
/**
 * Created by willemdejong on 20-01-17.
 */
document.addEventListener('DOMContentLoaded', () => {
    $.ajax({
        method: "get",
        url: "../getResult",
        data: { name: "acteurMeesteSlechteFilms" }
    })
        .done(function(input) {
            result = input;
            const index = result[0].actorname.indexOf(",");
            if(index > 0){
                $("#actorName")[0].innerHTML = result[0].actorname.substring(index+1, result[0].actorname.length) + " " + result[0].actorname.substring(0, index);
            }
            else {
                $("#actorName")[0].innerHTML = result[0].actorname;
            }

            $("#filmCount")[0].innerHTML = result[0].count;
            $("#filmRating")[0].innerHTML = Math.round((result[0].avgrating) * 100) / 100;
        });


});


