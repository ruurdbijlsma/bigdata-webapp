<%--suppress CssUnusedSymbol --%>

<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:layout title="Gemiddelde rating per genre" section="menu_visualisations">

  <jsp:attribute name="body_area">
    <h1 class="page-header">Visualisaties - Gemiddelde rating per genre</h1>
      <style>

          .bar {
              fill: steelblue;
          }

          .bar:hover {
              fill: brown;
          }

          .axis--x path {
              display: none;
          }

          .rtext{
              fill: white;
          }

      </style>
          <svg width="1080" height="500"></svg>
          <script src="https://d3js.org/d3.v4.min.js"></script>
          <script src="js/genrerating.js"></script>
</jsp:attribute>

</t:layout>
