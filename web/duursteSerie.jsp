<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:layout title="SQL Results - Duurste tv-serie" section="menu_sqlresults">

  <jsp:attribute name="body_area">
    <script src="js/duursteSeriescript.js"></script>
    <h1 class="page-header">SQL Results - Duurste tv-serie</h1>
        <div class="col-lg-8 col-lg-offset-2">
            <div class="col-lg-12 text-answer">De tv-serie <strong id="filmTitle"></strong>, is de duurste tv-serie geweest om op te nemen. De serie kostte maar liefst &euro;<strong id="filmBudget"></strong> euro om op te nemen.</div>
            <div class="question"><p>Welke tv-serie is het duurst geweest om op te nemen?</p></div>
        </div>

</jsp:attribute>

</t:layout>