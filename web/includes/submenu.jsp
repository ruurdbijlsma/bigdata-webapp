<li id="menu_overview"><a href="/">Overview <span class="sr-only">(current)</span></a></li>
<li id="menu_sqlresults" data-toggle="collapse" data-target="#sqlresults" class="collapsed"><a href="#">SQL Results</a>
</li>
<ul class="sub-menu collapse" id="sqlresults">
    <li class="active"><a href="meestOpgebrachtNL.jsp">Meest opgebrachte film NL</a></li>
    <li><a href="acteurMeesteSlechteFilms.jsp">Acteur meest slechte films</a></li>
    <li><a href="actorfilm.jsp">Acteur in Film</a></li>
    <li><a href="duursteSerie.jsp">Duurste tv-serie</a></li>
    <li><a href="frieseactor.jsp">Succesvolle Friezen</a></li>
</ul>
<li id="menu_visualisations" data-toggle="collapse" data-target="#visualisations" class="collapsed"><a href="#">Visualisations</a>
</li>
<ul class="sub-menu collapse" id="visualisations">
    <li class="active"><a href="moviemap.jsp">Movie Map</a></li>
    <li><a href="actormap.jsp">Actor Map</a></li>
    <li><a href="numbers.jsp">Title numbers</a></li>
    <li><a href="genrerating.jsp">Genre Rating</a></li>
    <li><a href="genrecost.jsp">Genre Kosten</a></li>
    <li><a href="wordcloud.jsp">Actor Cloud</a></li>
</ul>
<li id="menu_statistics" data-toggle="collapse" data-target="#statistics" class="collapsed"><a href="#">Statistics</a>
</li>
<ul class="sub-menu collapse" id="statistics">
    <li class="active"><a href="kosten.jsp">Kosten</a></li>
    <li><a href="succesmovie.jsp">Movie Succes</a></li>
</ul>