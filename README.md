# bigdata-webapp
1. Set Java SDK version to 1.8 in File>Project Structure>Project and Project Structure>Modules
2. Edit Configurations
3. Add local Tomcat server configuration
4. Configure application server
4. Set tomcat folder as Tomcat Home
5. Press "Fix"
5. Start PostgreSQL on port 5432
5. Create database called "IMDB"
6. Create schema called "staging" and one called "bigdata"
7. Create superuser called "bigdata" with password "bigdata"
8. Set permissions to read/write by Everyone on tomcat/bin/parser/output and tomcat/bin/sql/results
9. Have R Installed
9. (Windows) Add R to PATH
9. Set permissions to read/write on R installation folder
9. Run
10. Press Update DB to download the files and create the necessary tables
