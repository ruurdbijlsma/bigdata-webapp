package bigdata;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created by ruurd on 12-1-2017.
 */
class ParserManager {
    private final String outputFolder = "parser/output/";
    private final Parser[] parsers;

    ParserManager(boolean forceDownload, boolean forceParse, Parser... parsers) {
        this.parsers = parsers;

        ArrayList<Thread> threads = new ArrayList<>();
        long startTime = System.nanoTime();

        for (Parser parser : parsers) {
            if (forceParse | !Files.exists(Paths.get(outputFolder + parser.getListName() + ".csv"))) {

                Thread t = new Thread(() -> parser.start(forceDownload));
                threads.add(t);
                t.start();
            } else {
                parser.getList(false);
            }
        }

        try {
            for (Thread thread : threads) {
                thread.join();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        long endTime = System.nanoTime();
        float elapsedTime = (endTime - startTime) / 1000000000f;
        System.out.println("Complete time: " + elapsedTime + "s");
    }


    String getSQL() {
        List<String> sql = new ArrayList<>();
        String allTables = Arrays.stream(parsers).map(p -> "staging." + p.getTableName()).collect(Collectors.joining(", "));
        if (!Objects.equals(allTables, "")) {
            sql.add("DROP TABLE IF EXISTS " + allTables + " CASCADE;\n\n");
        }
        for (Parser parser : parsers) {
            String strategySQL = parser.getStrategy().getSQL();
            if (strategySQL != null) {
                if (sql.stream().collect(Collectors.joining("")).contains(strategySQL)) {
                    strategySQL = "COPY" + strategySQL.split("\nCOPY")[1];
                }
                sql.add(strategySQL + "FROM '" + parser.getOutputPath() + "' DELIMITER ';' CSV HEADER ENCODING 'LATIN1';\n\n");
            }
        }
        return sql.stream().collect(Collectors.joining(""));
    }
}
