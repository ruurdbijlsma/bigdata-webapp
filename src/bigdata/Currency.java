package bigdata;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Objects;

/**
 * Class to convert any currency to Euro
 */
public class Currency {
    private final static HashMap<String, Float> currencyToEuroMap = new HashMap<>();

    static {//CBT Currency doesnt exist
        //Save all currency values in a hashmap to reduce loading time
        currencyToEuroMap.put("VAL", 5.16457E-4f);
        currencyToEuroMap.put("FJD", 0.4495f);
        currencyToEuroMap.put("MXN", 0.0441f);
        currencyToEuroMap.put("LVL", 1.5278f);
        currencyToEuroMap.put("CDF", 8.0E-4f);
        currencyToEuroMap.put("GTQ", 0.1258f);
        currencyToEuroMap.put("CLP", 0.0014f);
        currencyToEuroMap.put("UGX", 3.0E-4f);
        currencyToEuroMap.put("HNL", 0.0412f);
        currencyToEuroMap.put("ZAR", 0.0685f);
        currencyToEuroMap.put("TND", 0.4117f);
        currencyToEuroMap.put("SDD", 0.00147763f);
        currencyToEuroMap.put("BSD", 0.9481f);
        currencyToEuroMap.put("IQD", 8.0E-4f);
        currencyToEuroMap.put("CUP", 0.9481f);
        currencyToEuroMap.put("TWD", 0.0295f);
        currencyToEuroMap.put("DOP", 0.0205f);
        currencyToEuroMap.put("MYR", 0.2119f);
        currencyToEuroMap.put("GEL", 0.3439f);
        currencyToEuroMap.put("UYU", 0.0331f);
        currencyToEuroMap.put("MAD", 0.0935f);
        currencyToEuroMap.put("AZM", 1.07104E-4f);
        currencyToEuroMap.put("OMR", 2.4598f);
        currencyToEuroMap.put("PGK", 0.2984f);
        currencyToEuroMap.put("SEK", 0.104f);
        currencyToEuroMap.put("KES", 0.0091f);
        currencyToEuroMap.put("UAH", 0.0353f);
        currencyToEuroMap.put("BTN", 0.0139f);
        currencyToEuroMap.put("MZM", 1.34188E-5f);
        currencyToEuroMap.put("ARS", 0.0596f);
        currencyToEuroMap.put("QAR", 0.26f);
        currencyToEuroMap.put("NLG", 0.45378f);
        currencyToEuroMap.put("IRR", 0.0f);
        currencyToEuroMap.put("CNY", 0.1368f);
        currencyToEuroMap.put("THB", 0.0264f);
        currencyToEuroMap.put("XPF", 0.0084f);
        currencyToEuroMap.put("BDT", 0.0121f);
        currencyToEuroMap.put("LYD", 0.6602f);
        currencyToEuroMap.put("PHP", 0.0191f);
        currencyToEuroMap.put("KWD", 3.09f);
        currencyToEuroMap.put("PYG", 2.0E-4f);
        currencyToEuroMap.put("ISK", 0.0083f);
        currencyToEuroMap.put("JMD", 0.0074f);
        currencyToEuroMap.put("BEF", 0.0247894f);
        currencyToEuroMap.put("USA", 0.957429f);
        currencyToEuroMap.put("ESP", 0.00601012f);
        currencyToEuroMap.put("COP", 3.0E-4f);
        currencyToEuroMap.put("USD", 0.9483f);
        currencyToEuroMap.put("MKD", 0.0163f);
        currencyToEuroMap.put("RUR", 1.6E-5f);
        currencyToEuroMap.put("DZD", 0.0086f);
        currencyToEuroMap.put("GGP", 1.18132f);
        currencyToEuroMap.put("SGD", 0.6585f);
        currencyToEuroMap.put("VEB", 9.62238E-5f);
        currencyToEuroMap.put("ETB", 0.0423f);
        currencyToEuroMap.put("KGS", 0.0137f);
        currencyToEuroMap.put("BND", 0.6587f);
        currencyToEuroMap.put("OGR", 0.0f);
        currencyToEuroMap.put("XAF", 0.0015f);
        currencyToEuroMap.put("LRD", 0.0102f);
        currencyToEuroMap.put("ATS", 0.0726728f);
        currencyToEuroMap.put("ITL", 6.0E-4f);
        currencyToEuroMap.put("CHF", 0.9318f);
        currencyToEuroMap.put("HRK", 0.1315f);
        currencyToEuroMap.put("ALL", 0.0074f);
        currencyToEuroMap.put("GHC", 2.28761E-5f);
        currencyToEuroMap.put("MTL", 2.32937f);
        currencyToEuroMap.put("TZS", 4.0E-4f);
        currencyToEuroMap.put("XAU", 1254.6007f);
        currencyToEuroMap.put("VND", 0.0f);
        currencyToEuroMap.put("TRL", 2.72134E-7f);
        currencyToEuroMap.put("AUD", 0.6947f);
        currencyToEuroMap.put("ILS", 0.2455f);
        currencyToEuroMap.put("BOB", 0.1382f);
        currencyToEuroMap.put("KHR", 2.0E-4f);
        currencyToEuroMap.put("MDL", 0.0473f);
        currencyToEuroMap.put("IDR", 1.0E-4f);
        currencyToEuroMap.put("KYD", 1.1561f);
        currencyToEuroMap.put("AMD", 0.002f);
        currencyToEuroMap.put("LBP", 6.0E-4f);
        currencyToEuroMap.put("CYP", 1.8248f);
        currencyToEuroMap.put("JOD", 1.3334f);
        currencyToEuroMap.put("HKD", 0.1223f);
        currencyToEuroMap.put("AED", 0.2581f);
        currencyToEuroMap.put("EUR", 1.0f);
        currencyToEuroMap.put("DKK", 0.1344f);
        currencyToEuroMap.put("BGL", 5.1198E-4f);
        currencyToEuroMap.put("ZWD", 0.00264538f);
        currencyToEuroMap.put("CAD", 0.7166f);
        currencyToEuroMap.put("EEK", 0.0639115f);
        currencyToEuroMap.put("MMK", 7.0E-4f);
        currencyToEuroMap.put("NOK", 0.1109f);
        currencyToEuroMap.put("MUR", 0.0263f);
        currencyToEuroMap.put("SYP", 0.0018f);
        currencyToEuroMap.put("IMP", 1.18159f);
        currencyToEuroMap.put("ROL", 2.20892E-5f);
        currencyToEuroMap.put("YUM", 0.51198006f);
        currencyToEuroMap.put("RON", 0.2216f);
        currencyToEuroMap.put("LKR", 0.0063f);
        currencyToEuroMap.put("NGN", 0.0031f);
        currencyToEuroMap.put("IEP", 1.3564f);
        currencyToEuroMap.put("CZK", 0.037f);
        currencyToEuroMap.put("CRC", 0.0017f);
        currencyToEuroMap.put("PKR", 0.009f);
        currencyToEuroMap.put("GRD", 0.0029347f);
        currencyToEuroMap.put("XCD", 0.3511f);
        currencyToEuroMap.put("HTG", 0.0145f);
        currencyToEuroMap.put("ANG", 0.5352f);
        currencyToEuroMap.put("AFA", 1.435E-5f);
        currencyToEuroMap.put("SIT", 0.0044f);
        currencyToEuroMap.put("BHD", 2.5118f);
        currencyToEuroMap.put("PTE", 0.00498798f);
        currencyToEuroMap.put("KZT", 0.0029f);
        currencyToEuroMap.put("LTL", 0.311f);
        currencyToEuroMap.put("TTD", 0.1422f);
        currencyToEuroMap.put("SAR", 0.2505f);
        currencyToEuroMap.put("YER", 0.0038f);
        currencyToEuroMap.put("MVR", 0.0615f);
        currencyToEuroMap.put("INR", 0.0139f);
        currencyToEuroMap.put("KRW", 8.0E-4f);
        currencyToEuroMap.put("NPR", 0.0087f);
        currencyToEuroMap.put("AWG", 0.5297f);
        currencyToEuroMap.put("JPY", 0.0081f);
        currencyToEuroMap.put("MNT", 4.0E-4f);
        currencyToEuroMap.put("PLN", 0.2285f);
        currencyToEuroMap.put("CBT", 0.0f);
        currencyToEuroMap.put("GBP", 1.1525f);
        currencyToEuroMap.put("HUF", 0.0032f);
        currencyToEuroMap.put("BYR", 0.0f);
        currencyToEuroMap.put("LUF", 0.0247894f);
        currencyToEuroMap.put("BIF", 6.0E-4f);
        currencyToEuroMap.put("FIM", 0.168188f);
        currencyToEuroMap.put("MGF", 5.72867E-5f);
        currencyToEuroMap.put("DEM", 0.5522f);
        currencyToEuroMap.put("BAM", 0.5101f);
        currencyToEuroMap.put("EGP", 0.0502f);
        currencyToEuroMap.put("MOP", 0.1187f);
        currencyToEuroMap.put("NAD", 0.0685f);
        currencyToEuroMap.put("SKK", 0.0331939f);
        currencyToEuroMap.put("TMM", 5.46736E-5f);
        currencyToEuroMap.put("NIO", 0.0323f);
        currencyToEuroMap.put("PEN", 0.2759f);
        currencyToEuroMap.put("NZD", 0.6614f);
        currencyToEuroMap.put("FRF", 0.1646f);
        currencyToEuroMap.put("BRL", 0.294f);
    }

    /**
     * @return gets generated hashmap as a string
     */
    public static String hashMapToString() {
        return currencyToEuroMap.toString();
    }

    /**
     * @param from  currency to exchange from
     * @param money currency to exchange to
     * @return value in Euro
     */
    public static float exchangeToEuro(String from, long money) {
        return money * getExchangeToEuro(from);
    }

    /**
     * @param from Currency to exchange from
     * @return exchange rate to euro
     */
    private static float getExchangeToEuro(String from) {
        if (!currencyToEuroMap.containsKey(from)) {
            try {
                float rate = quote(from, "EUR");
                currencyToEuroMap.put(from, rate);
            } catch (IOException e) {
                e.printStackTrace();
                return 1;
            }
        }
        return currencyToEuroMap.get(from);
    }

    /**
     * @param from currency to exchange from
     * @param to   currency to exchange to
     * @return exchange rate
     * @throws IOException this method uses Http, so IOExceptions could happen
     */
    private static float quote(String from, String to) throws IOException {
        StringBuilder result = new StringBuilder();
        URL url = new URL("http://quote.yahoo.com/d/quotes.csv?s=" + from + to + "=X&f=l1&e=.csv");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String line;
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        rd.close();
        if (Objects.equals(result.toString(), "N/A")) {
            System.out.println("Error, currency not found! " + from);
            return 0;
        }
        return Float.parseFloat(result.toString());
    }
}
