package bigdata;

import bigdata.ParseStrategies.IParserStrategy;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.lang.Thread.sleep;

/**
 * Parses imdb list files to csv
 */
class Parser {
    private final String inputFolder = "parser/input/";
    private final String outputFolder = "parser/output/";
    private final String listName;
    private final IParserStrategy strategy;
    private Path listPath;

    /**
     * @param strategy Strategy to use when parsing
     * @param listName Name of imdb list
     */
    Parser(IParserStrategy strategy, String listName) {
        this.listName = listName;
        this.strategy = strategy;

        if (!Files.exists(Paths.get(inputFolder))) {
            new File(inputFolder).mkdir();
        }
        if (!Files.exists(Paths.get(outputFolder))) {
            new File(outputFolder).mkdir();
        }
    }

    String getListName() {
        return listName;
    }

    String getTableName() {
        Matcher matcher = Pattern.compile(" staging.(.+)\\n").matcher(getStrategy().getSQL());
        if (matcher.find()) {
            return matcher.group(1);
        }

        String[] words = listName.split("-");
        String name = Arrays.stream(words).map(w -> w.substring(0, 1).toUpperCase() + w.substring(1)).collect(Collectors.joining(""));
        return name.substring(0, 1).toLowerCase() + name.substring(1);
    }

    IParserStrategy getStrategy() {
        return strategy;
    }

    String getOutputPath() {
        return listPath.toAbsolutePath().toString().replaceAll("input", "output").replaceAll(".list", ".csv");
    }

    boolean getList(boolean forceDownload) {
        boolean success = true;
        String input = inputFolder + listName + ".list";

        listPath = Paths.get(input);

        if (!Files.exists(listPath) || forceDownload) {
            success = ListGetter.get(listName);
            listPath = Paths.get(input);
        }
        return success;
    }

    void start(boolean forceDownload) {
        while (!getList(forceDownload))
            try {
                System.out.println("Error: Downloading " + listName + " failed, trying again...");
                sleep(new Random().nextInt(15000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        long startTime = System.nanoTime();
        System.out.println("Starting parse on " + listName + "... ");

        OutputStream outputStream = new OutputStream(strategy.getColumnNames(), listName);

        CharsetDecoder dec = StandardCharsets.ISO_8859_1.newDecoder().onMalformedInput(CodingErrorAction.IGNORE);

        try (Reader r = Channels.newReader(FileChannel.open(listPath), dec, -1); BufferedReader br = new BufferedReader(r)) {
            outputStream = strategy.parse(br, outputStream);
            outputStream.closeFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        long endTime = System.nanoTime();
        float elapsedTime = (endTime - startTime) / 1000000000f;
        System.out.println("Parsing of " + listName + " took " + elapsedTime + "s");
    }
}
