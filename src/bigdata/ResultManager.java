package bigdata;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by ruurd on 12-1-2017.
 */
public class ResultManager {
    private static ResultManager instance = null;
    private final String inputFolder = "sql/queries/";
    private final String outputFolder = "sql/results/";
    private final PostgreSQL connection;

    private ResultManager() {
        connection = PostgreSQL.getInstance();
    }

    public static ResultManager getInstance() {
        if (instance == null) {
            instance = new ResultManager();
        }
        return instance;
    }

    private void updateAllResults() {
        File folder = new File(inputFolder);
        File[] listOfFiles = folder.listFiles();

        assert listOfFiles != null;
        for (File file : listOfFiles) {
            if (file.isFile()) {
                String fileName = file.getName();
                fileName = fileName.substring(0, fileName.length() - 4);
                updateResult(fileName);
            }
        }
    }

    public void updateAllResultsAsync() {
        new Thread(this::updateAllResults).start();
    }

    private boolean updateResult(String name) {
        String queryPath = inputFolder + name + ".sql";
        if (Files.exists(Paths.get(queryPath))) {
            String query = GetFileContent.get(queryPath);
            connection.exportToJSON(query, name);
            return true;
        }
        return false;
    }

    public String getResult(String name) {
        String resultPath = outputFolder + name + ".json";

        if (!Files.exists(Paths.get(resultPath)))
            if (!updateResult(name))
                return "Error could not find sql query for question: " + name;

        return GetFileContent.get(resultPath);
    }
}
