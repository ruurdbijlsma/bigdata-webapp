package bigdata;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

class StreamHog extends Thread {
    private final InputStream is;
    private final boolean capture;

    StreamHog(InputStream is, boolean capture) {
        this.is = is;
        this.capture = capture;
        start();
    }

    public void run() {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line;
            while ((line = br.readLine()) != null) {
                if (capture) { // we are supposed to capture the output from REG command
                    int i = line.indexOf("InstallPath");
                    if (i >= 0) {
                        String s = line.substring(i + 11).trim();
                        int j = s.indexOf("REG_SZ");
                        if (j >= 0)
                            s = s.substring(j + 6).trim();
                        System.out.println("R InstallPath = " + s);
                    }
                } else
                    System.out.println("Rserve>" + line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}