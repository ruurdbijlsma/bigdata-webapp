package bigdata;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created by ruurd on 16-1-2017.
 */
public class GetFileContent {
    public static String get(String path) {
        FileInputStream fis;
        String result = "";
        try {
            File file = new File(path);
            fis = new FileInputStream(file);
            byte[] data = new byte[(int) file.length()];
            fis.read(data);
            fis.close();

            result = new String(data, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }
}
