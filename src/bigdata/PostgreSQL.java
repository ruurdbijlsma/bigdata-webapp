package bigdata;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.util.Objects;

/**
 * Created by ruurd on 12-1-2017.
 */
public class PostgreSQL {
    private static PostgreSQL instance = null;
    private final String outputFolder = "sql/results/";
    private Connection connection = null;

    private PostgreSQL() {
        String database = "IMDB",
                username = "bigdata",
                password = "bigdata",
                port = "5432";
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager
                    .getConnection("jdbc:postgresql://localhost:" + port + "/" + database,
                            username, password);
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }

        System.out.println("Opened database successfully");
    }

    public static PostgreSQL getInstance() {
        if (instance == null) {
            instance = new PostgreSQL();
        }
        return instance;
    }

    Boolean exists(String table) {
        String result = executeQuery("SELECT *\n" +
                "FROM " + table + "\n" +
                "LIMIT 1;");

        return !Objects.equals(result, "");
    }

    public String executeQuery(String query) {
        System.out.println("Executing new sql query...");
        long startTime = System.nanoTime();
        String textResponse = "";

        try {
            Statement statement = connection.createStatement();
            statement.executeQuery("SELECT array_to_json(array_agg(row_to_json(t))) FROM (" + query + ") t");
            ResultSet result = statement.getResultSet();
            result.next();
            textResponse += result.getString(1);
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        long endTime = System.nanoTime();
        float elapsedTime = (endTime - startTime) / 1000000000f;
        System.out.println("Executing query took: " + elapsedTime + "s");

        return textResponse;
    }

    public void executeUpdate(String query) {
        System.out.println("Executing new sql update...");
        long startTime = System.nanoTime();

        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(query);
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        long endTime = System.nanoTime();
        float elapsedTime = (endTime - startTime) / 1000000000f;
        System.out.println("Executing update took: " + elapsedTime + "s");
    }

    void exportToJSON(String query, String path) {
        path = outputFolder + path + ".json";
        Path parent = Paths.get(path).getParent();
        if (!Files.exists(parent)) {
            try {
                Files.createDirectory(parent);
                System.out.println("Please set proper permissions on " + parent.toAbsolutePath() + "\nQuery can not execute if without write permissions");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Path p = Paths.get(path).toAbsolutePath();
            executeUpdate("COPY(\n" +
                    " SELECT array_to_json(array_agg(row_to_json(t))) FROM (\n" +
                    query + ") t\n" +
                    ") TO '" + p + "';");
            System.out.println("Succesfully exported results of query to " + p);
        }
    }
}
