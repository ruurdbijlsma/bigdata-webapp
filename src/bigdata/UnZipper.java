package bigdata;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;


/**
 * Unzips gzip files
 */
class UnZipper {
    private static final String inputFolder = "parser/input/";

    /**
     * @param listName name of imdb list
     */
    static void unzip(String listName) {
        String gzipInput = inputFolder + listName + ".list.gz";
        String listOutput = inputFolder + listName + ".list";

        byte[] buffer = new byte[1024];

        try {
            FileInputStream inputStream = new FileInputStream(gzipInput);
            GZIPInputStream gStream = new GZIPInputStream(inputStream);
            FileOutputStream out = new FileOutputStream(listOutput);

            int len;
            while ((len = gStream.read(buffer)) > 0) {
                out.write(buffer, 0, len);
            }

            inputStream.close();
            gStream.close();
            out.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
