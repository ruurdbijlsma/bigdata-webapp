package bigdata;

import sun.net.ftp.FtpLoginException;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import static java.lang.Thread.sleep;

/**
 * Retrieves .list files from IMDB and uncompresses them
 */
class ListGetter {
    private static final String inputFolder = "parser/input/";
    private static final int maximumParallelDownloads = 5;
    private static final AtomicInteger parallelDownloads = new AtomicInteger(0);

    /**
     * @param listName name of list
     */
    static boolean get(String listName) {
        if (parallelDownloads.get() < maximumParallelDownloads) {
            try {
                int pdpd = parallelDownloads.incrementAndGet();
                System.out.println("Incrementing pd to: " + pdpd);

                String gzipLocation = inputFolder + listName + ".list.gz";
                System.out.println("Starting " + listName + " download...");

                URL site = new URL("ftp://ftp.fu-berlin.de/pub/misc/movies/database/" + listName + ".list.gz");
                ReadableByteChannel byteChannel = Channels.newChannel(site.openStream());
                FileOutputStream fileOutputStream = new FileOutputStream(gzipLocation);
                fileOutputStream.getChannel().transferFrom(byteChannel, 0, Long.MAX_VALUE);
                fileOutputStream.close();

                pdpd = parallelDownloads.decrementAndGet();
                System.out.println("Decrementing pd to: " + pdpd);

                System.out.println("Downloaded " + listName + " successfully, starting decompression...");
                UnZipper.unzip(listName);

                Files.delete(Paths.get(gzipLocation));

                System.out.println("Decompressed " + listName + " successfully");
                return true;
            } catch (FtpLoginException e) {
                return false;//Teveel parallel downloads, fu-berlin.de wil niet meer
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                System.out.println("Maximum downloads exceeded, waiting 20s to try " + listName + " again.");
                sleep(15000 + new Random().nextInt(30000));
                return get(listName);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return false;
    }
}
