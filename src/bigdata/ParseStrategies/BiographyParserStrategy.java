package bigdata.ParseStrategies;

import bigdata.OutputStream;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * This class converts the biographies.list file to a .csv file.
 * It stores the name, birthdate (if available) and birthplace (if available)
 *
 * @author Mitchell van der Galiën
 */
public class BiographyParserStrategy implements IParserStrategy {
    private static BiographyParserStrategy instance = null;

    public static BiographyParserStrategy getInstance() {
        if (instance == null) {
            instance = new BiographyParserStrategy();
        }
        return instance;
    }

    @Override
    public String[] getColumnNames() {
        return new String[]{"Name", "BirthDate", "BirthPlace"};
    }

    @Override
    public String getSQL() {
        return "CREATE TABLE staging.biographies\n" +
                "(\n" +
                "  id    SERIAL NOT NULL  PRIMARY KEY,\n" +
                "  name TEXT,\n" +
                "  birthDate  TEXT,\n" +
                "  birthPlace  TEXT\n" +
                ");\n" +
                "\n" +
                "COPY staging.biographies (name, birthDate, birthPlace)\n";
    }

    @Override
    public OutputStream parse(BufferedReader reader, OutputStream result) {
        String name = "NULL", birthDate = "NULL", birthPlace = "NULL";
        Boolean newPerson = false, started = false;
        try {
            String line;
            while ((line = reader.readLine()) != null) {
                line = line.replaceAll("\"", "");
                line = line.replaceAll(";", ",");
                if (line.equals("-------------------------------------------------------------------------------")) {
                    if (started) {
                        result.addRow(name, birthDate, birthPlace);
                    }
                    if (!started) {
                        started = true;
                    }
                    newPerson = true;
                    name = "NULL";
                    birthDate = "NULL";
                    birthPlace = "NULL";
                }
                if (newPerson) {
                    if (line.contains("NM:")) {
                        name = line.substring(4);
                        newPerson = false;
                    }
                }
                if (line.contains("DB:")) {
                    line = line.replaceAll(";", ",");
                    int index = line.indexOf(',');

                    //check if there are numbers on the line, if so there should be a birthdate
                    if (line.matches(".*\\d+.*")) {

                        if (index < 0) {
                            birthDate = line.substring(4);
                        } else {
                            birthDate = line.substring(4, line.indexOf(','));
                            //System.out.println(birthPlace); 
                        }
                    }
                    if (index > 0) {
                        birthPlace = line.substring(line.indexOf(',') + 1);
                    }

                    //fix for birthplace if the person was born in a country or city that doesn't exist anymore or has a different name
                    if (birthPlace.contains("[now")) {
                        String birthPlace_cp = birthPlace;
                        birthPlace = birthPlace.substring(0, birthPlace.indexOf(',') + 1);
                        birthPlace = birthPlace + birthPlace_cp.substring(birthPlace_cp.indexOf('[') + 4).replaceAll("]", "");
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}
