package bigdata.ParseStrategies;

import bigdata.OutputStream;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * ActorParserStrategy
 * Parse actors.list to actors.csv
 * Parse actresses.list to actresses.csv
 *
 * @author willemdejong
 */

public class ActorParserStrategy implements IParserStrategy {
    private static ActorParserStrategy instance = null;

    public static ActorParserStrategy getInstance() {
        if (instance == null) {
            instance = new ActorParserStrategy();
        }
        return instance;
    }

    @Override
    public String[] getColumnNames() {
        return new String[]{"Actor", "Movie"};
    }

    @Override
    public String getSQL() {
        return "CREATE TABLE staging.actors\n" +
                "(\n" +
                "  id    SERIAL NOT NULL  PRIMARY KEY,\n" +
                "  name TEXT,\n" +
                "  title  TEXT\n" +
                ");\n" +
                "\n" +
                "COPY staging.actors (name, title)\n";
    }

    @Override
    public OutputStream parse(BufferedReader reader, OutputStream result) {
        boolean started = false;
        String[] startOfFile = new String[]{"Name\t\t\tTitle", "----\t\t\t-----"};
        String prevLine = "";
        String endOfFile = "----------------------------------------------------------------------------";
        try {
            String line, title = "NULL", actor = "NULL", previousTitle = "";
            while ((line = reader.readLine()) != null) {
                line = line.replaceAll(";", "");
                if (started && line.contains(endOfFile))
                    break;
                if (started && !line.isEmpty()) {
                    if (!line.startsWith("\t")) {
                        //new actor found
                        if (line.contains("\t") || line.contains("  ")) {
                            //parse actor and his movie
                            line = line.replaceAll("  ", "\t");
                            Matcher oneMovieMatcher = Pattern.compile("([^\\t]+)\\t+(.+)").matcher(line);
                            if (oneMovieMatcher.find()) {
                                actor = oneMovieMatcher.group(1).trim();
                                title = oneMovieMatcher.group(2).trim();
                            }
                        } else {
                            System.out.println("FAILED to seperate actor and movie");
                        }
                    } else {
                        //movie found for current actor
                        title = line;
                    }

                    String yearMatch = "????";
                    int index = title.indexOf(yearMatch);
                    String reverseTitle = new StringBuilder(title).reverse().toString();
                    title = title.replaceAll("\"", "").trim();
                    Matcher yearFinder = Pattern.compile("(\\d{4})\\(").matcher(reverseTitle);
                    if (index == -1 && yearFinder.find()) {
                        yearMatch = "(" + new StringBuilder(yearFinder.group(1)).reverse().toString();
                        index = title.indexOf(yearMatch);
                    }
                    int cutoffIndex = index + yearMatch.length();
                    if (cutoffIndex <= title.length())
                        title = title.substring(0, cutoffIndex) + ")";

                    title = title.replaceAll("\\{.+}", "");
                    title = title.replaceAll("\\[.+]", "");
                    title = title.replaceAll("<.+>", "").trim();
                    if (!Objects.equals(title, previousTitle)) {
                        result.addRow(actor, title);
                        previousTitle = title;
                    }
                }
                started = started || line.contains(startOfFile[1]) && prevLine.contains(startOfFile[0]);
                prevLine = line;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}