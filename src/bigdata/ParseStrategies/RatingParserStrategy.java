package bigdata.ParseStrategies;

import bigdata.OutputStream;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class converts the ratings.list file to a .csv file.
 * It stores the rating and number of votes for each movie/tv series.
 *
 * @author Mitchell van der Galiën
 */
public class RatingParserStrategy extends MovieStringParser implements IParserStrategy {
    private static RatingParserStrategy instance = null;

    public static RatingParserStrategy getInstance() {
        if (instance == null) {
            instance = new RatingParserStrategy();
        }
        return instance;
    }

    @Override
    public String[] getColumnNames() {
        return new String[]{"Movie", "Rating", "Votes"};
    }

    @Override
    public String getSQL() {
        return "CREATE TABLE staging.ratings\n" +
                "(\n" +
                "  id    SERIAL NOT NULL  PRIMARY KEY,\n" +
                "  title TEXT,\n" +
                "  rating  FLOAT,\n" +
                "  votes  int\n" +
                ");\n" +
                "\n" +
                "COPY staging.ratings (title, rating, votes)\n";
    }

    @Override
    public OutputStream parse(BufferedReader reader, OutputStream result) {
        String title = "init", rating, votes, sTitle;
        String pattern = "\\s+([0-9.*]+)\\s+(\\d+)\\s+(\\d+\\.\\d)\\s+(.*)";
        Pattern p = Pattern.compile(pattern);
        String line;
        int index = 0;
        try {
            while ((line = reader.readLine()) != null) {
                index++;
                if (index > 293) {
                    Matcher m = p.matcher(line);

                    if (m.find()) {
                        if (title.length() < m.group(4).replaceAll("\"", "").length()) {
                            sTitle = m.group(4).replaceAll("\"", "").substring(0, title.length());
                            if (title.equals(sTitle)) {
                                continue;
                            }
                        }
                        title = m.group(4).replaceAll("\"", "").replaceAll(";", ",");
                        String[] output = parseTitle(title);
                        title = output[0];
                        rating = m.group(3);
                        votes = m.group(2);
                        result.addRow(title, rating, votes);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}
