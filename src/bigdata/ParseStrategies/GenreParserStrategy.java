package bigdata.ParseStrategies;

import bigdata.OutputStream;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * Converts Genres.list to genres.csv with a title (name + year) and genre (multiple genres takes multiple rows)
 *
 * @author Jeroen Jonker
 */
public class GenreParserStrategy implements IParserStrategy {

    private static GenreParserStrategy instance = null;

    public static GenreParserStrategy getInstance() {
        if (instance == null) {
            instance = new GenreParserStrategy();
        }
        return instance;
    }

    @Override
    public String[] getColumnNames() {
        return new String[]{"Title", "Genre"};
    }

    @Override
    public String getSQL() {
        return "CREATE TABLE staging.genres\n" +
                "(\n" +
                "  id    SERIAL NOT NULL  PRIMARY KEY,\n" +
                "  title TEXT,\n" +
                "  genre TEXT\n" +
                ");\n" +
                "\n" +
                "COPY staging.genres (title, genre)\n";
    }

    @Override
    public OutputStream parse(BufferedReader reader, OutputStream result) {
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                line = line.trim();
                line = line.replaceAll(";", ",");
                if (line.matches(".*(?=([(](((\\d){4}([/](I){1,4}+(V)?)?)|(\\?){4})[)])).*")) {

                    line = new StringBuilder(line).reverse().toString();
                    String[] yearSplit = line.split("(?=([)]((((V)?+(I){1,4}[/])?(\\d){4})|(\\?){4})[(]))", 2);

                    if (yearSplit.length == 2) {
                        //checks if not an example
                        if (yearSplit[0].contains("|"))
                            continue;

                        String[] whitespaceSplit = yearSplit[0].replaceAll("[{].*[}]", "").split("(\\s+)");

                        String title = new StringBuilder(yearSplit[1]).reverse().toString();
                        String genre = new StringBuilder(whitespaceSplit[0]).reverse().toString();

                        result.addRow(title, genre);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}