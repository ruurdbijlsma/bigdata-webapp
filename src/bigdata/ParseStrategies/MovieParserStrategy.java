package bigdata.ParseStrategies;

import bigdata.OutputStream;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Parse movies.list to movies.csv
 *
 * @author Ruurd Bijlsma
 */

public class MovieParserStrategy extends MovieStringParser implements IParserStrategy {
    private static MovieParserStrategy instance = null;

    public static MovieParserStrategy getInstance() {
        if (instance == null) {
            instance = new MovieParserStrategy();
        }
        return instance;
    }

    @Override
    public String[] getColumnNames() {
        return new String[]{"Title", "SeperateTitle", "Year", "Type"};
    }

    @Override
    public String getSQL() {
        return "CREATE TABLE staging.movies\n" +
                "(\n" +
                "  id    SERIAL NOT NULL  PRIMARY KEY,\n" +
                "  title TEXT,\n" +
                "  seperateTitle TEXT,\n" +
                "  year  TEXT,\n" +
                "  type  TEXT\n" +
                ");\n" +
                "\n" +
                "COPY staging.movies (title, seperateTitle, year, type)\n";
    }

    @Override
    public OutputStream parse(BufferedReader reader, OutputStream result) {
        boolean started = false;
        String startOfFile = "===========";
        String endOfFile = "--------------------------------------------------------------------------------";
        String previousTitle = "";

        String line;
        try {
            while ((line = reader.readLine()) != null) {
                line = line.replaceAll(";", ",");
                if (Objects.equals(line, endOfFile))
                    break;

                String title, year, type;
                if (started && !line.isEmpty()) {
                    String[] output = parseTitle(line);
                    title = output[0];
                    year = output[1];
                    type = output[2];

                    if (!Objects.equals(title, previousTitle)) {
                        String yearMatch = "(????";
                        int index = title.indexOf(yearMatch);
                        String reverseTitle = new StringBuilder(title).reverse().toString();
                        Matcher yearFinder = Pattern.compile("(\\d{4})\\(").matcher(reverseTitle);
                        if (index == -1 && yearFinder.find()) {
                            yearMatch = "(" + new StringBuilder(yearFinder.group(1)).reverse().toString();
                            index = title.indexOf(yearMatch);
                        }
                        String seperateTitle = "";
                        if (index <= title.length()) {
                            if (index <= 0)
                                System.out.println("hoi");
                            seperateTitle = title.substring(0, index - 1);
                        }

                        result.addRow(title, seperateTitle, year, type);
                        previousTitle = title;
                    }
                }

                started = started || line.contains(startOfFile);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}