package bigdata.ParseStrategies;

import bigdata.OutputStream;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * CountryParserStrategy
 * Parse country.list to country.csv
 *
 * @author willemdejong
 */

public class CountryParserStrategy implements IParserStrategy {
    private static CountryParserStrategy instance = null;

    public static CountryParserStrategy getInstance() {
        if (instance == null) {
            instance = new CountryParserStrategy();
        }
        return instance;
    }

    @Override
    public String[] getColumnNames() {
        return new String[]{"Title", "Country"};
    }

    @Override
    public String getSQL() {
        return "CREATE TABLE staging.countries\n" +
                "(\n" +
                "  id    SERIAL NOT NULL  PRIMARY KEY,\n" +
                "  country TEXT,\n" +
                "  title  TEXT\n" +
                ");\n" +
                "\n" +
                "COPY staging.countries (title, country)\n";
    }

    @Override
    public OutputStream parse(BufferedReader reader, OutputStream result) {
        boolean started = false;
        String startOfFile = "==============";
        String endOfFile = "--------------------------------------------------------------------------------";
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                line = line.replaceAll(";", "");
                line = line.replaceAll("\\{.+}", "");

                if (Objects.equals(line, endOfFile))
                    break;
                if (started && !line.isEmpty()) {
                    String regex = "^([\\s\\S]*)\\(([\\d{4}]*|\\?*)(?:/)?([\\w]*)?\\)(\\s*\\{([\\w!\\s:;/.\\-'\"?`_&@$%^*<>~+=|,()]*)(\\s*\\(#([\\d]*)\\.([\\d]*)\\))?})?\\s*([\\d{4}]*)?(?:-)?([\\d{4}]*)?\\s+(.*)";
                    Pattern pattern = Pattern.compile(regex);
                    Matcher matcher = pattern.matcher(line);
                    String country = "NULL",
                            title = "NULL";

                    if (matcher.find()) {

                        String titleYear = matcher.group(2);
                        title = matcher.group(1);
                        country = matcher.group(11);

                        title = title.trim();
                        titleYear = titleYear.trim();
                        title = title.replaceAll("\"", "");
                        if (!titleYear.isEmpty()) {
                            title = title + " (" + titleYear + ")";
                        }

                    }

                    result.addRow(title, country);
                }
                started = started || line.contains(startOfFile);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}