package bigdata.ParseStrategies;

import bigdata.OutputStream;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Objects;

/**
 * Converts running-times.list to running-times.csv with a title (name + year) and running time (in minutes)
 *
 * @author Jeroen Jonker
 */
public class RunningTimeParserStrategy extends MovieStringParser implements IParserStrategy {

    private static RunningTimeParserStrategy instance = null;

    public static RunningTimeParserStrategy getInstance() {
        if (instance == null) {
            instance = new RunningTimeParserStrategy();
        }
        return instance;
    }

    @Override
    public String[] getColumnNames() {
        return new String[]{"Title", "Time"};
    }

    @Override
    public String getSQL() {
        return "CREATE TABLE staging.runningTimes\n" +
                "(\n" +
                "  id    SERIAL NOT NULL  PRIMARY KEY,\n" +
                "  title TEXT,\n" +
                "  time  FLOAT\n" +
                ");\n" +
                "\n" +
                "COPY staging.runningTimes (title, time)\n";
    }

    @Override
    public OutputStream parse(BufferedReader reader, OutputStream result) {
        String line, previousLine = "";
        try {
            while ((line = reader.readLine()) != null) {
                line = line.trim();

                if (line.matches(".*(?=([(](((\\d){4}([/](I){1,4}+(V)?)?)|(\\?){4})[)])).*")) {
                    String[] yearSplit = line.replaceAll(";", ":").split("(?=([(](((\\d){4}([/](I){1,4}+(V)?)?)|(\\?){4})[)]))");
                    String[] whitespaceSplit = yearSplit[1].replaceAll("[{].*[}]", "").split("(\\s+)");
                    int whitespaceSplitLength = whitespaceSplit.length;

                    for (int x = 1; x < whitespaceSplitLength; x++) {
                        whitespaceSplit[x] = whitespaceSplit[x].replaceAll(",", ".").replaceAll("[^0-9.]", "").trim().replaceAll("\\.+$", "").replaceAll("^\\.+", "");
                        if (whitespaceSplit[x].matches(".*[0-9].*")) {
                            String title = yearSplit[0].replaceAll("\"", "") + whitespaceSplit[0];

                            String[] output = parseTitle(title);
                            title = output[0];

                            if (!Objects.equals(previousLine, title)) {
                                result.addRow(title, whitespaceSplit[x]);
                                previousLine = title;
                            }
                            break;
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}

