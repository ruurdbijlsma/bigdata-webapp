package bigdata.ParseStrategies;

import bigdata.OutputStream;

import java.io.BufferedReader;

/**
 * Parser strategy interface
 */
public interface IParserStrategy {
    String[] getColumnNames();

    String getSQL();

    OutputStream parse(BufferedReader reader, OutputStream result);
}