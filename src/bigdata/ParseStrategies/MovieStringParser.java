package bigdata.ParseStrategies;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Abstract class to parse strings containing movie titles
 */
abstract class MovieStringParser {
    /**
     * K
     *
     * @param movieString string to parse
     * @return list of strings containing Title, Year and Type of the show
     */
    String[] parseTitle(String movieString) {
        String title = "NULL", titleYear = "????", year = "NULL", type = "MOVIE";

        movieString = movieString.trim().replaceAll("\\{.+}", "");

        String regex = "^([\\s\\S]*)\\(([\\d{4}]*|\\?*)(?:/)?([\\w]*)?\\)(\\s*\\{([\\w!\\s:;/.\\-'\"?`_&@$%^*<>~+=|,()]*)(\\s*\\(#([\\d]*)\\.([\\d]*)\\))?})?\\s*([\\d{4}]*)?(?:-)?([\\d{4}]*)?";
        Matcher matcher = Pattern.compile(regex).matcher(movieString);

        if (matcher.find()) {
            title = matcher.group(1);
            titleYear = matcher.group(2);
            year = matcher.group(9);
        }

        if (!year.matches("\\d{4}"))
            year = titleYear.matches("\\d{4}") ? titleYear : "NULL";

        title = title.trim();
        titleYear = titleYear.trim();

        if (title.startsWith("\""))
            type = "SERIE";

        if (movieString.contains("(VG)"))
            type = "VIDEOGAME";

        title = title.replaceAll("\"", "");

        if (!titleYear.isEmpty())
            title = title + " (" + titleYear + ")";

        return new String[]{title, year, type};
    }
}