package bigdata.ParseStrategies;

import bigdata.Currency;
import bigdata.OutputStream;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Parse business.list to business.csv
 *
 * @author Ruurd Bijlsma
 */

public class BusinessParserStrategy extends MovieStringParser implements IParserStrategy {
    private static BusinessParserStrategy instance = null;

    public static BusinessParserStrategy getInstance() {
        if (instance == null) {
            instance = new BusinessParserStrategy();
        }
        return instance;
    }


    @Override
    public String[] getColumnNames() {
        return new String[]{"Title", "Budget", "GrossRevenue"};
    }

    @Override
    public String getSQL() {
        return "CREATE TABLE staging.business\n" +
                "(\n" +
                "  id      SERIAL NOT NULL  PRIMARY KEY,\n" +
                "  title   TEXT,\n" +
                "  budget  BIGINT,\n" +
                "  revenue BIGINT\n" +
                ");\n" +
                "\n" +
                "COPY staging.business (title, budget, revenue)\n";
    }

    @Override
    public OutputStream parse(BufferedReader reader, OutputStream result) {
        boolean started = false;
        String[] startOfFile = new String[]{"BUSINESS LIST", "============="};
        String prevLine = "",
                endOfFile = "Corrections can be made by using the keyword",
                splitter = "---------------------------------------------------------------------------",
                currentTitle = "",
                prevTitle = "";
        long budget = 0, grossRevenue = 0;
        boolean foundBudget = false,
                foundGross = false;

        String line, currentCountry = "";
        try {
            while ((line = reader.readLine()) != null) {
                line = line.replaceAll(";", ",");
                if (line.contains(endOfFile))
                    break;

                if (started && !line.isEmpty()) {
                    if (line.contains("MV: ")) {
                        String[] output = parseTitle(line.substring(4));
                        currentTitle = output[0];
                    }
                    boolean containsBT = line.contains("BT: "),
                            containsGR = line.contains("GR: ");
                    if (containsBT | containsGR) {
                        String firstNumber = line.replaceAll("[^\\d+| ]", "");
                        Matcher numberGroupMatcher = Pattern.compile("\\d+").matcher(firstNumber);
                        if (numberGroupMatcher.find()) {
                            firstNumber = numberGroupMatcher.group(0);
                            if (!firstNumber.isEmpty()) {
                                String currency = "USD", country = "USA";

                                Matcher matcher = Pattern.compile("[A-Z]{3}").matcher(line);
                                if (matcher.find())
                                    currency = matcher.group(0);

                                if (containsBT) {
                                    foundBudget = true;
                                    budget = (long) Currency.exchangeToEuro(currency, Long.parseLong(firstNumber));
                                } else {
                                    Matcher countryMatcher = Pattern.compile("\\(([A-z]+)\\)").matcher(line);
                                    if (countryMatcher.find())
                                        country = countryMatcher.group(0);

                                    if (!Objects.equals(currentCountry, "(Worldwide)") && !Objects.equals(country, currentCountry)) {
                                        currentCountry = country;
                                        foundGross = true;
                                        if (Objects.equals(country, "(Worldwide)")) {
                                            grossRevenue = (long) Currency.exchangeToEuro(currency, Long.parseLong(firstNumber));
                                        } else {
                                            grossRevenue += Currency.exchangeToEuro(currency, Long.parseLong(firstNumber));
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (line.contains(splitter)) {
                        if ((foundBudget || foundGross) && !Objects.equals(currentTitle, prevTitle)) {
                            result.addRow(currentTitle, budget, grossRevenue);
                        }
                        currentTitle = "";
                        budget = 0;
                        grossRevenue = 0;
                        foundBudget = false;
                        foundGross = false;
                        currentCountry = "";
                    }
                }
                started = started || line.contains(startOfFile[1]) && prevLine.contains(startOfFile[0]);
                prevLine = line;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }
}
