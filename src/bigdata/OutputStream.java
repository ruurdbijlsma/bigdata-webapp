package bigdata;

import java.io.FileWriter;
import java.io.IOException;

/**
 * Helper class to output csv to a stream
 */
public class OutputStream {
    ;
    private final String outputFolder = "parser/output/";
    private final String[] columnNames;
    private FileWriter output;

    /**
     * @param columnNames names of columns
     * @param listName    name of imdb list
     */
    public OutputStream(String[] columnNames, String listName) {
        this.columnNames = columnNames;
        try {
            this.output = new FileWriter(outputFolder + listName + ".csv");
            this.output.write(String.join(";", columnNames) + "\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param row objects to add to csv table
     */
    public void addRow(Object... row) {
        if (row.length == columnNames.length) {
            try {
                String rowResult = "";
                for (Object item : row) {
                    rowResult += item.toString().trim() + ";";
                }
                output.write(rowResult.substring(0, rowResult.length() - 1) + "\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Error: Row count does not match given names count");
        }
    }

    /**
     * Closes file stream
     */
    void closeFile() {
        try {
            output.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
