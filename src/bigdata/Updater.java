package bigdata;

import bigdata.ParseStrategies.*;


public class Updater {
    private static void updateDatabase() {
        ParserManager parsers = new ParserManager(
                false,
                true,
                new Parser(BiographyParserStrategy.getInstance(), "biographies"),
                new Parser(BusinessParserStrategy.getInstance(), "business"),
                new Parser(CountryParserStrategy.getInstance(), "countries"),
                new Parser(GenreParserStrategy.getInstance(), "genres"),
                new Parser(MovieParserStrategy.getInstance(), "movies"),
                new Parser(RatingParserStrategy.getInstance(), "ratings"),
                new Parser(ActorParserStrategy.getInstance(), "actors"),
                new Parser(ActorParserStrategy.getInstance(), "actresses"),
                new Parser(RunningTimeParserStrategy.getInstance(), "running-times")
        );

        PostgreSQL connection = PostgreSQL.getInstance();

        String query = parsers.getSQL();
        connection.executeUpdate(query);
//
//        query = GetFileContent.get("StagingDBQuery.sql");
//        connection.executeUpdate(query);
    }

    public static void updateDatabaseAsync() {
        Thread thread = new Thread(Updater::updateDatabase);
        thread.start();
    }
}
