package bigdata;

import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.RList;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by Jeroen on 25-1-2017.
 * !R + library Rserve required for Output R!
 */
public class Statistics {
    private static Statistics instance = null;
    private final String inputFolder = "statistics/scripts/";
    private final String outputFolder = "statistics/results/";
    private RConnection connection = null;

    private Statistics() {
        try {
            String killCommand, startCommand;
            boolean isWindows = System.getProperty("os.name").contains("Windows");
            String installDependenciesCommand = "source('" + toRPath(inputFolder + "dependancies.R") + "');";
            if (isWindows) {
                killCommand = "taskkill /im rserve.exe /f";
                startCommand = "R -e \"" + installDependenciesCommand + "library(Rserve);Rserve();\"";
            } else {
                killCommand = "killall Rserve";
                startCommand = "R -e " + installDependenciesCommand + " -e library(Rserve) -e Rserve(args=\"--vanilla\")";
            }
            Process killProcess = Runtime.getRuntime().exec(killCommand);
            killProcess.waitFor();

            Process startProcess = Runtime.getRuntime().exec(startCommand);
            new StreamHog(startProcess.getErrorStream(), false);
            new StreamHog(startProcess.getInputStream(), false);

            if (!isWindows) /* on Windows the process will never return, so we cannot wait */
                startProcess.waitFor();

            int attempts = 10; /* try up to 5 times before giving up. We can be conservative here, because at this point the process execution itself was successful and the start up is usually asynchronous */
            while (attempts > 0) {
                try {
                    connection = new RConnection();
                    System.out.println("R is running, Java has succesfully connected.");
                    break;
                } catch (Exception e2) {
                    System.out.println("Try failed with: " + e2.getMessage());
                }
            /* a safety sleep just in case the start up is delayed or asynchronous */
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException ignored) {
                }
                attempts--;
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static Statistics getInstance() {
        if (instance == null) {
            instance = new Statistics();
        }
        return instance;
    }

    private String toRPath(String path) {
        Path relativePath = Paths.get(path);
        String absolutePath = relativePath.toAbsolutePath().toString();
        return absolutePath.replaceAll("\\\\", "/");
    }

    private REXP eval(String command) {
        REXP result = null;
        try {
            result = connection.eval(command);
        } catch (RserveException e) {
            e.printStackTrace();
        }
        return result;
    }

    private String getEvalResult(String command) {
        try {
            return eval(command).asString();
        } catch (REXPMismatchException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void writeToFile(String output, String fileName) {
        fileName = outputFolder + fileName;
        if (!Files.exists(Paths.get(outputFolder))) {
            new File(outputFolder).mkdir();
        }

        try {
            FileWriter writer = new FileWriter(fileName);
            writer.write(output);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String updateStatistic(String function) {
        String summary = getFunctionSummary(function);
        writeToFile(summary, function + ".txt");
        return summary;
    }

    public String getResult(String function) {
        return GetFileContent.get(outputFolder + function + ".txt");
    }

    public String getFunctionSummary(String function) {
        System.out.println("Started executing R function: " + function + "...");
        long startTime = System.nanoTime();

        String rCommand = "source('" + toRPath(inputFolder + "Rquestions.R") + "')";
        eval(rCommand);
        String listString = "";
        RList resultList;
        try {
            resultList = eval(function + "()").asList();

            for (Object listElement : resultList) {
                listString += ((REXP) listElement).asString() + "$$$$$$";
            }

        } catch (REXPMismatchException e) {
            e.printStackTrace();
        }

        long endTime = System.nanoTime();
        float elapsedTime = (endTime - startTime) / 1000000000f;
        System.out.println("Finished R function: " + function + "! It took: " + elapsedTime + "s");

        return listString;
    }
}
