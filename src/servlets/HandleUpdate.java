package servlets;

import bigdata.ResultManager;
import bigdata.Statistics;
import bigdata.Updater;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet("/update")
public class HandleUpdate extends HttpServlet {
    @Override
    protected synchronized void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");

        response.setContentType("application/text");
        String textResponse;
        switch (action) {
            case "database":
                Updater.updateDatabaseAsync();
                textResponse = "Database will now update";
                break;
            case "results":
                ResultManager.getInstance().updateAllResultsAsync();
                textResponse = "Results will now update";
                break;
            case "statistics":
                textResponse = Statistics.getInstance().updateStatistic("kosten")+"\n\n";
                textResponse += Statistics.getInstance().updateStatistic("SuccesMovie");
                break;
            default:
                textResponse = "Wrong action";
                break;
        }
        response.getWriter().write(textResponse);
    }
}