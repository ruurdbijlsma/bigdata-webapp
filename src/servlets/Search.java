package servlets;

import bigdata.PostgreSQL;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet("/search")
public class Search extends HttpServlet {
    @Override
    protected synchronized void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String query = request.getParameter("query");

        response.setContentType("application/json");
        query = query.toLowerCase();
        String textResponse = PostgreSQL.getInstance().executeQuery("SELECT *\n" +
                "  FROM bigdata.show\n" +
                "  WHERE lower(title) LIKE '%" + query + "%' AND votes IS NOT NULL\n" +
                "  ORDER BY votes DESC\n" +
                "  LIMIT 5");
        response.getWriter().write(textResponse);
    }
}