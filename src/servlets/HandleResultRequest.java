package servlets;

import bigdata.ResultManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet("/getResult")
public class HandleResultRequest extends HttpServlet {
    @Override
    protected synchronized void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");

        response.setContentType("application/json");

        String textResponse;
        ResultManager manager = ResultManager.getInstance();
        textResponse = manager.getResult(name);

        response.getWriter().write(textResponse);
    }
}