package servlets;

import bigdata.PostgreSQL;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet("/actor")
public class GetActors extends HttpServlet {
    @Override
    protected synchronized void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String movie = request.getParameter("movie");

        response.setContentType("application/json");
        movie = movie.toLowerCase();
        String query = "SELECT actorname\n" +
                "FROM bigdata.show\n" +
                "  LEFT JOIN bigdata.playsin ON bigdata.show.title = bigdata.playsin.showtitle\n" +
                "WHERE lower(title) = '" + movie + "'";
        String textResponse = PostgreSQL.getInstance().executeQuery(query);
        response.getWriter().write(textResponse);
    }
}
